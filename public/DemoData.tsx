import {QuestionnaireCampaign} from "../types/dashboard";

export const GeneralService:any = {


    getWalletData() {
        return [
            {
                id: '1000',
                transactionId: 'f230fh0g3',
                name: 'Chinedu Wilbert',
                amount: 6500,
                status: "failed",
                gateway: "PayStack",
                date: "20-10-2023",
            },
            {
                id: '1001',
                transactionId: 'h0g3f230f',
                code: 'nvklal433',
                name: 'Okafor John',
                amount: 7500,
                status: "successes",
                gateway: "FlutterWave",
                date: "20-10-2023",
            },
        ]
    },

    getContactGroup() {
        return [
            {
                id: 1,
                Contacts: 10,
                groupName: 'KreativeRock',
                date: "20-10-2023",
                contactDetails:[
                    {
                        id: 11,
                        fullName:"Bianca Ojukwu",
                        email:"bibi@gmail.com",
                        phone:"080912345879",
                        date: "10-10-2023",
                    },
                    {
                        id: 22,
                        fullName:"Ojukwu Bianca ",
                        email:"bibid@gmail.com",
                        phone:"080912335879",
                        date: "10-10-2023",
                    }
                ]
            },
            {
                id: 2,
                Contacts: 22,
                groupName: 'KreativeRock',
                date: "20-10-2023",
                contactDetails:[]
            },
            {
                id: 3,
                Contacts: 32,
                groupName: 'Kreative-Tesst',
                date: "10-10-2023",
                contactDetails:[]
            },

        ]
    },

    getSenderMaskData() {
        return [
            {
                senderId: 'sjdkhkjs',
                userName: 'Chinedu Wilbert',
                senderMask: 'SenderMask testing One',
                status: "pending",
                date: "20-10-2023",
            },
            {
                senderId: 'shkuhskdh',
                userName: 'Chinedu Wilbert',
                senderMask: 'SenderMask testing two',
                status: "approved",
                date: "10-10-2023",
            },
        ]
    },

    getMediaMaskData() {
        return [
            {
                mediaId: 'sjdkhkjs',
                userName: 'Chinedu Wilbert',
                mediaMask: 'MediaMask testing One',
                status: "pending",
                date: "20-10-2023",
            },
            {
                mediaId: 'shkuhskdh',
                userName: 'Chinedu Wilbert',
                mediaMask: 'MediaMask testing two',
                status: "approved",
                date: "10-10-2023",
            },
        ]
    },



    // SMS
    getSmsReportBarData() {
        return [
            {
                success: 3,
                title: "success Messages",
            },
            {
                fail: 4,
                title: "failed Messages",
            },
            {
                sent: 2,
                title: "sent Messages",
            },
            {
                unitCost: 60,
                title: "SMS Cost",

            },
        ]
    },

    getSmsReportData() {
        return [
            {
                reportId: "wjhfrenfkej",
                senderMask: "Test One",
                Recipient: "08187832884345",
                content: "testing conversation demo",
                status: "failed",
                cost: 0,
                sendingTime: "18-12-2021",
                finishingTime: "20-12-2021",
            },
            {
                reportId: "wjhfrenfkej",
                senderMask: "Test Two",
                Recipient: "08187832884345",
                content: "testing conversation",
                status: "success",
                cost: 2,
                sendingTime: "18-12-2021",
                finishingTime: "20-12-2021",
            },

        ]
    },


    // VOICE
    getVoiceFileData() {
        return [
            {
                voiceId: 'sjdkhkjs',
                fileName: 'voice one',
                voiceFile: 'MediaMask testing One',
                date: "20-10-2023",
            },
            {
                voiceId: 'shkuhskdh',
                fileName: 'voice two',
                voiceFile: 'MediaMask testing two',
                date: "10-10-2023",
            },

        ]
    },

    getVoiceReportBarData() {
        return [
            {
                success: 3,
                title: "success Messages",
            },
            {
                fail: 4,
                title: "failed Messages",
            },
            {
                sent: 2,
                title: "sent Messages",
            },
            {
                unitCost: 60,
                title: "SMS Cost",

            },
        ]
    },

    getVoiceReportData() {
        return [
            {
                taskId: "8973huwwcd",
                voiceId: "wjhfrenfkej",
                campaignName: "Test One",
                mediaMask: "Test One",
                callingNumber: "08187832884345",
                Recipient: "08187832884345",
                content: "testing conversation demo",
                status: "failed",
                cost: 0,
                sendingTime: "18-12-2021",
                finishingTime: "20-12-2021",
            },
            {
                taskId: "8973huwwcd",
                voiceId: "wjhfrenfkej",
                campaignName: "Test One",
                mediaMask: "Test One",
                callingNumber: "08187832884345",
                Recipient: "08187832884345",
                content: "testing conversation demo",
                status: "success",
                cost: 20,
                sendingTime: "18-12-2021",
                finishingTime: "20-12-2021",
            },

        ]
    },

    // QUESTIONNAIRE
    getQuestionnaireCampaignData() {
        return [
            {
                id: 'sjdkhkjs',
                name: 'whatsapp one',
                date: "20-10-2023",
            },
            {
                id: 'shkuhskdh',
                name: 'whatsapp two',
                date: "10-10-2023",
            },
        ]
    },


    // TWO - WAY - SMS

    getTwoWaySMsChat() {
       return [
           {
               name: "Martin",
               phone: "+234918765456567",
               conversations: [
                   {
                       sender: "admin",
                       message: "That pizza place was amazing! We should go again sometime. 🍕",

                   },
                   {
                       sender: "Martin",
                       response: "Sure, what do you want to talk about?",
                   },

               ],
           },
           {
               name: "Charlie",
               phone: "+234918765456567",
               conversations: [
                   {
                       sender: "admin",
                       message: "Hey, do you have any recommendations for a good movie to watch?",
                   },
                   {
                       sender: "Charlie",
                       response: "Sure, have you watched 'The Shawshank Redemption'?",
                   },
               ],
           },
       ];
    },



    getSmsChatDetails() {
        return Promise.resolve(this.getTwoWaySMsChat().slice(0, 10));
    },

    //WALLET

    getWalletDetails() {
        return Promise.resolve(this.getWalletData().slice(0, 10));
    },

    //  CONTACT UPLOAD & FILE
    getContactGroupDetails() {
        return Promise.resolve(this.getContactGroup().slice(0, 10));
    },


    //  MEDIA & SENDER MASK
    getSenderMaskDetails() {
        return Promise.resolve(this.getSenderMaskData().slice(0, 10));
    },

    getMediaMaskDetails() {
        return Promise.resolve(this.getMediaMaskData().slice(0, 10));
    },

    // SMS FILE

    getSmsReportBarDataDetails() {
        return Promise.resolve(this.getSmsReportBarData().slice(0, 10));
    },

    getSmsReportDetails() {
        return Promise.resolve(this.getSmsReportData().slice(0, 10));
    },




    // VOICE SMS  & FILE
    getVoiceFileDetails() {
        return Promise.resolve(this.getVoiceFileData().slice(0, 10));
    },

    getVoiceReportBarDetails() {
        return Promise.resolve(this.getVoiceReportBarData().slice(0, 10));
    },

    getVoiceReportDetails() {
        return Promise.resolve(this.getVoiceReportData().slice(0, 10));
    },


    // QUESTIONNAIRE

    getQuestionnaireCampaignDetails() {
        return Promise.resolve(this.getQuestionnaireCampaignData().slice(0, 10));
    },
}