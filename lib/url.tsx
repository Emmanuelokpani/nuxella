export const API_URL = process.env.NEXT_PUBLIC_API_URL
export const CLIENT_URL = process.env.NEXT_PUBLIC_CLIENT_URL
export const NEXT_PUBLIC_AUTH_KEY = process.env.NEXT_PUBLIC_AUTH_KEY

export const NEXT_PUBLIC_FLUTTER_WAVE_TEST_PUBLIC_KEY = process.env.NEXT_PUBLIC_FLUTTER_WAVE_TEST_PUBLIC_KEY
export const NEXT_PUBLIC_PAY_STACK_PUBLIC_KEY = process.env.NEXT_PUBLIC_PAY_STACK_PUBLIC_KEY
