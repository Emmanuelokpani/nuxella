import Image from "next/image";
import React from "react";
import styled from "styled-components";

interface CardProps {
    subTitle?: string;
    title: string;
    content: string;
    icon?: any;
    index:number
}

export default function Card_NSK(props: CardProps) {
    const {subTitle, title, content, icon, index} = props;

    return (
        <CardContainer className="grid" key={index}>
            <div className="col-12 md:col-12 lg:p-1">
                <div className="flex  justify-content-center mb-3">
                    <Image src={icon} className="mr-3" alt=""/>
                </div>
                <div className="flex justify-content-center surface-border">
                    <span className="text-sm text-900 font-bold">{subTitle}</span>
                </div>
                <div className="flex justify-content-center surface-border mb-2 pb-2">
                    <span className="text-3xl text-900 font-bold">{title}</span>
                </div>
                <TextDiv className="text-md text-900 mt-2">{content}</TextDiv>
            </div>
        </CardContainer>
    );
}

const CardContainer = styled.div`
    width: 100%;
    margin-bottom: 0 !important;
    display: flex;
    flex-direction: column;
    text-align: start;
    //align-items: center;
    //justify-content: center;

`;

const TextDiv = styled.div`
    color: #6f7070;
    //text-align: center;
`;
