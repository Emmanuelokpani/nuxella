import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { InputTextarea } from "primereact/inputtextarea";
import { CustomButton } from "./Styled_Button";
import React, { useRef } from 'react';
import { useRouter } from "next/navigation";
import styled from "styled-components";
import 'react-phone-input-2/lib/style.css'
import {useFormik} from "formik";
import { useMutation } from '@apollo/client';
import { BUSINESS_ENQUIRY } from '../graphql/mutations/UserSettings';
import { Toast } from 'primereact/toast';
import { createUserEnquirySchema } from '../../validation';


const Services = [
  { value: "ONE_WAY_MOBILE_TEXTING", name: "one way mobile texting" },
  { value: "TWO_WAY_MOBILE_TEXTING", name: "two way mobile texting" },
  { value: "WHATSAPP_BUSINESS", name: "whatsapp business" },
  { value: "SPONSORED_ADS", name: "sponsored ads" },
];



const Form_Input = () => {
    const toast = useRef<Toast | any>(null);
  const router = useRouter();


    const [CreateUserEnquiry, {loading, data}] = useMutation<any>(BUSINESS_ENQUIRY, {
        awaitRefetchQueries: true,
        onCompleted: () => {
            console.log(data,"completed");
            toast?.current?.show({ severity: 'info', summary: 'success', detail: `Service Enquiry sent successfully`, life: 30000});
            formik.resetForm()
            router.push("/feedback");
        },
    })


    const formik = useFormik({
        initialValues: {
            fullName: '',
            organizationName: '',
            email: '',
            service: '',
            website: '',
            descriptions:'',
        },

        validationSchema:createUserEnquirySchema,
        onSubmit: values => {
            console.log({ values });
            CreateUserEnquiry({
                variables: {
                    data:{
                        fullName:values.fullName,
                        organizationName:values.organizationName,
                        email:values.email,
                        service:values.service,
                        website:values.website,
                        descriptions:values.descriptions
                    },
                }
            })
                .then(({ data }) => {
                    console.log(data);
                })
        },
    })

    return (
        <Forms_Style className="grid formgrid p-fluid mt-6">
          <div className="field mb-4 col-12 md:col-6">
            <label htmlFor="Full Name" className="font-medium text-900">
              Full Name
            </label>
            <InputText
                type="text"
                placeholder="full name"
                id="fullName"  name="fullName"
                value={formik.values.fullName}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                className={`w-full pl-9 pr-3 py-3 rounded-lg border-1 outline-none text-md ${
                  formik.errors.fullName && formik.touched.fullName
                      ? "border-red-500" : "border-green-500" }`}
            />
          </div>
          <div className="field mb-4 col-12 md:col-6">
            <label htmlFor="Organization Name" className="font-medium text-900">
              Organization Name
            </label>
              <InputText
                  type="text"
                  placeholder="organization name"
                  id="organizationName"  name="organizationName"
                  value={formik.values.organizationName}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  className={`w-full pl-9 pr-3 py-3 rounded-lg border-1 outline-none text-md ${
                      formik.errors.organizationName && formik.touched.organizationName
                          ? "border-red-500" : "border-green-500" }`}
              />
          </div>
          <div className="field mb-4 col-12 md:col-12">
            <label htmlFor="Email" className="font-medium text-900">
              Email
            </label>
              <InputText
                  type="text"
                  placeholder="email"
                  id="name"  name="email"
                  value={formik.values.email}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  className={`w-full pl-9 pr-3 py-3 rounded-lg border-1 outline-none text-md ${
                      formik.errors.email && formik.touched.email
                          ? "border-red-500" : "border-green-500" }`}
              />
          </div>
          <div className="field mb-4 col-12 md:col-6">
            <label htmlFor="Service Needed" className="font-medium text-900 ">
              Service
            </label>
            <Dropdown
                options={Services}
                optionLabel="name"
                placeholder="select a service"
                type="text"
                id="service"  name="service"
                value={formik.values.service}
                onBlur={formik.handleBlur}
                // onChange={formik.handleChange}
                onChange={(e) => formik.setFieldValue("service", e.value)}
                className={`w-full pl-9 pr-3 py-1 rounded-lg border-1 outline-none text-md ${
                    formik.errors.email && formik.touched.email
                        ? "border-red-500" : "border-green-500" }`}
            />
          </div>

          <div className="field mb-4 col-12 md:col-6">
            <label htmlFor="Website" className="font-medium text-900">
              Website (https://www.website.com)
            </label>
            <InputText
                       type="text"
                       placeholder="https://www.website.com"
                       id="website"  name="website"
                       value={formik.values.website}
                       onBlur={formik.handleBlur}
                       onChange={formik.handleChange}
                       className={`w-full pl-9 pr-3 py-3 rounded-lg border-1 outline-none text-md ${
                           formik.errors.website && formik.touched.website
                               ? "border-red-500" : "border-green-500" }`}
            />
          </div>

          <div className="field mb-4 col-12">
            <label htmlFor="bio1" className="font-medium text-900">
              Descriptions
            </label>
            {/*  descriptions*/}
            <InputTextarea
                rows={5}
                autoResize={true}
                id="descriptions"  name="descriptions"
                placeholder='service description'
                value={formik.values.descriptions}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                className={`w-full pl-9 pr-3 py-4 rounded-lg border-1 outline-none text-md ${
                    formik.errors.descriptions && formik.touched.descriptions
                        ? "border-red-500" : "border-green-500" }`}
            />
          </div>

          <ButtonContainer>
            <CustomButton

                label={loading ? 'Please Wait' : 'sms enquiry'}
                onClick={() => formik.handleSubmit()}
                className="custom-blue-button "
                icon="add"
                color="#fff"
                background="#FF7900"
            />
          </ButtonContainer>
        </Forms_Style>
    );
};
const ButtonContainer = styled.div`
  
  width: 100%;
  display: flex;
  align-items:center;
  justify-content: center;
`
export const Forms_Style = styled.div`
    box-shadow: 0px 8px 24px 0px rgba(140, 149, 159, 0.2);
    padding: 1rem;
   
  .p-inputtext:enabled:hover {
    border-color: #495057;
  }

  .p-inputtext:enabled:focus {
    outline: 0 none;
    outline-offset: 0;
    box-shadow: none;
    border-color: #495057;
  }

  .PhoneInputCountry {
    background: #ffffff;
    margin-right: 0.02em;
    padding: 8px 16px;
    border-radius: 15px 1px 1px 15px;
    border: 1px solid #ced4da;
  }

  .PhoneInputCountrySelect {
    padding: 0.5rem;
  }

  .PhoneInputInput {
    padding: 15px;
    border-radius: 1px 15px 15px 1px;
    border: 0.7px solid #ced4da;
  }
  .PhoneInputInput:enabled:focus {
    //border-color: #495057;
    //border-color: #495057;
    //border-color: #495057;
    border: 0.1px solid #495057;
  }
`;

export default Form_Input;
