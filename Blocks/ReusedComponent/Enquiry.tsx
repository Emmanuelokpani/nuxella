import React, {useRef} from "react";
import * as yup from "yup";
import {useMutation} from "@apollo/client";
import {FormikHelpers} from "formik";
import {Toast} from "primereact/toast";
import CustomForm, {IFormInput, IFormValues} from "../utils/Form/CustomForm";
import TextInput from "../utils/Inputs/TextInput";
import {BUSINESS_ENQUIRY, REQUEST_SENDER_MASK} from "../graphql/mutations/UserSettings";
import {USER_SENDER_MASK} from "../graphql/queries";



const inputList: Array<IFormInput> = [
    {
        name: 'smsMask',
        as: TextInput,
        format: (value:any) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'createList', className: 'w-full md:w-64 lg:w-90 mb-6 ' },
        placeholder: 'request sender mask',
        label: '* Sender Mask'
    },

    {
        name: 'smsMask',
        as: TextInput,
        format: (value:any) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'createList', className: 'w-full md:w-64 lg:w-90 mb-6 ' },
        placeholder: 'request sender mask',
        label: '* Sender Mask'
    },
];
const schema = yup.object({
    smsMask: yup.string()
        .required('sender mask title is required')
        .test('start-with-lowercase', 'Must start with a lowercase letter', (value) => {
            if (!value) return true; // Already covered by required
            return /^[a-z]/.test(value);
        }),
});




const User_Enquiry = ()=>{

    const toast = useRef<any | null>(null);
    const [CreateUserEnquiry, { loading }] = useMutation(BUSINESS_ENQUIRY, {
        refetchQueries: [USER_SENDER_MASK], awaitRefetchQueries: true,
        onCompleted: (data) => {
            toast.current.show({severity: 'success', summary: 'success', detail: `${data?.requestSenderMask}`, life: 40000});
            console.log(data, "data")
        },
        onError: (error) => {
            toast.current.show({
                severity: 'warn',
                summary: 'Error',
                detail: `${error}`,
                life: 30000,
            });
        },
    })


    const handleSubmit = async (payload: IFormValues<IFormInput>, helpers: FormikHelpers<IFormValues<IFormInput>>) => {
        console.log(payload, "before execution")
        CreateUserEnquiry({variables: {data:{...payload}}})
            .then((data) => {
                helpers.resetForm();
                console.log(data, "sent")
            }).catch((error)=>{
            console.log( error?.message, "error message")
        });
    };


    return(
        <div className='w-full'>
            <Toast ref={toast} />
            <CustomForm
                validateOnMount={false}
                enableReinitialize
                initialValues={{ smsMask: ''}}
                validationSchema={schema}
                onSubmitFunction={handleSubmit}
                // loading={loading}
                formInputs={inputList}
                formButtonProps={{ className: 'w-15rem flex justify-between align-content-end text-lg text-white font-bold' }}
                submitButtonText="request"
            />
        </div>
    )
}

export default User_Enquiry