import Image from "next/image";
import React from "react";
import styled from "styled-components";
import Avatar3 from "../../public/image/page/avatar3.png";

interface CardProps {
  title: string;
  content: string;
  icon: any;
  index:number
}

export default function Card(props: CardProps) {
  const { title, content, icon, index} = props;

  return (
      <CardContainer className="card lg:ml-4" key={index}>
        <div className="flex flex-row  mb-3">
          <Image src={icon} width={100} height={100} className="mr-3" alt="1" />
        </div>
        <div className="flex flex-row text-start mb-2 pb-2">
          <span className="text-md font-bold mb-1 ">{title}</span>
        </div>
        <TextDiv className="text-sm">{content}</TextDiv>
      </CardContainer>
  );
}

 const CardContainer = styled.div`
     width: 95%;
     height:35vh;
     margin-bottom: 0!important;
     display: flex;
     flex-direction: column;
     text-align:start;
      border:1px solid #0778FD;
      background-color:#000000;
     color:#FFFFFF;
`;



export const TextDiv = styled.div`
  text-align: start;
`;
