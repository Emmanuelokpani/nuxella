import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

interface CustomButtonProps {
  label?:string;
  onClick?:()=> void;
  className?:string;
  icon?:any;
  color?:string;
  background?:string;
  disabled?:boolean
}
export const CustomButton = ({ label, onClick, className, icon,color,background, disabled }: CustomButtonProps) => {
  return (
    <StyledButton className={`custom-button ${className}`} color={color} background={background} onClick={onClick}>
       <IconWrapper>{icon}</IconWrapper>
      <IconWrapper>{label}</IconWrapper>
    </StyledButton>
  );
};


CustomButton.propTypes = {
  label: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
  icon: PropTypes.string,
  style: PropTypes.object,
};

const StyledButton = styled.button<{color:string,background:string}>`
  //font-family: 'Archivo-Narrow';
  font-family: 'Archivo-Narrow', sans-serif;
  cursor: pointer;
  flex-shrink: 0;
  border-radius: 10px;
  //background: #ff7900;
  background:${(props)=>props.background?props.background:"#AEDFD4"};

  // background: ${(props)=>props.background};
  color: ${(props)=>props.color};
  //color: ${(props)=>props.color};
  box-shadow: 0px 1px 4px 0px rgba(0, 0, 0, 0.25);
  border: none;
  z-index: 1;
  padding:12px;
  //width: 200px;
  //height: 46px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: normal;
  text-transform: uppercase;

  @media (max-width: 768px) {
    font-size: 14px;
    padding: 8px 16px;
  }
`;

const IconWrapper = styled.div`
  display: flex;
  align-items: center;

  .icon {
    margin-right: 4px;
    font-size: 1.6rem;
  }
`;

