import {Skeleton} from "primereact/skeleton";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";

export const UserLoading = ( )=>{
    return(
        <div className=' p-4 '>
            <Skeleton width="15rem" className="mb-2"></Skeleton>
            <Skeleton width="10rem" className="mb-2"></Skeleton>
            <Skeleton width="10rem"></Skeleton>
        </div>
    )
}


export const TableLoading = ( )=>{
    const items:any = Array.from({ length: 5 }, (v, i) => i);

    const bodyTemplate = () => {
        return <Skeleton></Skeleton>
    }
    return(
        <div className=' p-4 '>
            <DataTable value={items} className="p-datatable-striped">
                <Column field="code"  style={{ width: '25%' }} body={bodyTemplate}></Column>
                <Column field="name"  style={{ width: '25%' }} body={bodyTemplate}></Column>
                <Column field="category"  style={{ width: '25%' }} body={bodyTemplate}></Column>
                <Column field="quantity" style={{ width: '25%' }} body={bodyTemplate}></Column>
            </DataTable>
        </div>
    )
}



export  const ListLoading = ( )=>{
    return(
        <ul className="m-0 p-0 list-none">
            <li className="mb-3">
                <div className="flex p-4">
                    <Skeleton shape="circle" size="2rem" className="mr-2 border-circle" ></Skeleton>
                    <div style={{ flex: '1' }}>
                        <Skeleton width="75%" className='mt-2'></Skeleton>
                    </div>
                </div>
            </li>
        </ul>
    )
}