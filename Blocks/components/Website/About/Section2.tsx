'use client';
import HeroImg from '../../../../public/image/page/about2.png';
import Image from 'next/image';
import styled from 'styled-components';
import { useRouter } from 'next/navigation';
import HeroBackground from '../../../../public/image/website/landing_page/heroBackground.png';
import React from "react";
import {CustomButton} from "../../../ReusedComponent/Styled_Button";
import {Button} from "primereact/button";




const SectionTwo = () => {
    const router = useRouter();

    const NewRoute = () => {
        router.push('/service');
    };

    return (

        <Wrapper>
            <Container id="hero-section-container" >
                <SecondContainer>
                    <Image
                        width={400}
                        height={550}
                        src={HeroImg.src}
                        alt="hero-1"
                        className="hidden md:block"
                    />
                </SecondContainer>
                <FirstContainer>
                    <div className="lg:mt-8 justify-content-center lg:mb-8">
                        <h1 className=" text-4xl font-bold mt-0 mb-0">
                            We are Concerned about
                        </h1>
                        <h1 className=" text-4xl font-bold mt-0 mb-0 text-blue-500">
                            the End Result
                        </h1>


                        <p className="lg:mt-6">
                            At <span className="text-blue-500">Nuxalle</span>, we are committed to delivering the
                            results you deserve. We are a results-oriented team, and our most important question is
                            always,
                            What works? Every time we have asked that question, we have found the answer.
                        </p>

                        <p className="lg:mt-6">
                            That is exactly why we are your best option. We either know what works, or we know how to
                            figure it out. We have done it time and time again, consistently achieving the outcomes our
                            clients need. With
                            <span className="text-blue-500">  Nuxalle</span>,
                            you can trust that we will do whatever it takes to deliver the best results.
                        </p>
                        <p className="lg:mt-6">Exciting, right? We think so too. Now, imagine all the amazing things we
                            could achieve together.</p>

                        <div className="flex">
                            <Button
                                label="Our Services"
                                style={{
                                    background: "#000000", color: '#ffffff',
                                    border: "none", height: "2.6rem"
                                }}/>
                        </div>
                    </div>


                </FirstContainer>

            </Container>


        </Wrapper>
    );
};


const Wrapper = styled.div`
    margin-top:8rem;
    padding: 1rem 6rem;
    background-color: #ffffff;
    color: #000000;
    @media (max-width: 658px) {
        margin-top:1rem;
        padding: 0.1rem 0.1rem;

    }

`
const Container = styled.div`
    width: 100%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: grid;
    gap: 4rem;
    grid-template-columns: 1fr 1.5fr;
    align-items: center;
    justify-content: center;
    //padding-bottom: 2rem;
    
    
    @media (max-width: 1088px) {
      grid-template-columns: 1fr;
      max-height:max-content;
      padding: 2rem;
      //padding-top: 6rem;
  
 
  }
  //@media (max-width: 1000px) {
  //  min-height: max-content;
  //
  //
  //
  //}
  @media (max-width: 658px) {
    //padding: 2rem;
    //padding-top: 4rem;
    
  }
   
`;
const FirstContainer = styled.div`
  width:100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 3rem;
  @media (max-width: 1088px) {
      align-items: center;
  }
`

const SecondContainer = styled.div`
  width:100%;
  display: flex;
  /* align-items: center; */
  justify-content: center;
  @media (max-width: 1088px) {
      align-items: center;
  }
    
`

const InnerContainer = styled.div`
  margin-top:5rem;
    display: grid;
  grid-template-columns: 1fr 1fr 1fr;
 
  gap: 2rem;
  @media (max-width:1000px) {
   grid-template-columns: 1fr 1fr;
   
  }
  @media (max-width:670px) {
   grid-template-columns: 1fr;
   gap: 1rem;
  }
`

export default SectionTwo;
