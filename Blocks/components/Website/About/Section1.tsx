'use client';
import HeroImg from '../../../../public/image/page/about1.png';
import Image from 'next/image';
import styled from 'styled-components';
import { useRouter } from 'next/navigation';
import HeroBackground from '../../../../public/image/website/landing_page/heroBackground.png';
import React from "react";
import {CustomButton} from "../../../ReusedComponent/Styled_Button";
import {Button} from "primereact/button";




const SectionOne = () => {
    const router = useRouter();

    const NewRoute = () => {
        router.push('/');
        console.log('clicked');
    };

    return (
        <Wrapper>
            <Container id="hero-section-container">
                <FirstContainer>
                    <div className="lg:mt-8  justify-content-center lg:mb-8">
                        <h1 className=" text-4xl font-bold mt-0 mb-0">
                            How we <span className="text-blue-500 p-2">started</span> doing
                        </h1>

                        <h1 className=" text-4xl font-bold mt-0 mb-0">
                            what we <span className="text-blue-500 p-2">love</span>
                        </h1>

                        <p className="lg:mt-6">
                            We were just a bunch of Computer Science students, bound by a shared passion for innovation.
                            Our minds were constantly buzzing with ideas, and we
                            could not stop thinking about what to build next. It was not
                            long before we realized that this was not just a phase—it was who we were
                        </p>

                        <p className="lg:mt-6">
                            With that realization, we took a leap of faith and decided to create something meaningful.
                            It started with a small group of friends,
                            but our enthusiasm was contagious. Soon,
                            family members and even strangers joined our journey.
                        </p>
                        <p className="lg:mt-6">
                            Today, that small group has grown into Nuxalle—a
                            company dedicated to helping others bring their ideas, products, and businesses to life.
                            We are still driven by the same passion that brought
                            us together, and we are excited to see what the future holds.
                        </p>

                        <div className="flex  lg:mt-4">
                            <Button
                                label="Our Manifesto"
                                style={{
                                    background: "#000000", color: '#ffffff',
                                    border: "none", height: "2.6rem"
                                }}/>
                        </div>


                    </div>
                </FirstContainer>
                <SecondContainer>
                    <Image
                        width={400}
                        height={550}
                        src={HeroImg.src}
                        alt="hero-1 "
                        className="image"
                    />
                </SecondContainer>
            </Container>


        </Wrapper>
    );
};


const Wrapper = styled.div`
    margin-top:4rem;
    padding: 3rem 6rem;
    background-color: #ffffff;
    color:#000000;
    @media (max-width: 658px) {
        padding: 4rem 2rem;

    }
`
const Container = styled.div`
    width: 100%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: grid;
    gap: 4rem;
    grid-template-columns: 1.3fr 1fr;
    align-items: center;
    justify-content: center;
    //padding-bottom: 2rem;
    
    
    @media (max-width: 1088px) {
      grid-template-columns: 1fr;
      max-height:max-content;
      //padding: 4rem;
     
      //padding-top: 6rem;
  }
  //@media (max-width: 1000px) {
  //  min-height: max-content;
  //    
  //}
  @media (max-width: 658px) {
      //padding: 4rem 6rem;
    //padding-top: 4rem;
    
  }
   
`;
const FirstContainer = styled.div`
  width:100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 3rem;
  @media (max-width: 1088px) {
      align-items: center;
  }
`

const SecondContainer = styled.div`
  width:100%;
  display: flex;
  justify-content: center;


    @media (max-width:670px) {
       .image{
           width:100%;
       }
    }
    
    
`

const InnerContainer = styled.div`
  margin-top:5rem;
    display: grid;
  grid-template-columns: 1fr 1fr 1fr;
 
  gap: 2rem;
  @media (max-width:1000px) {
   grid-template-columns: 1fr 1fr;
   
  }
  @media (max-width:670px) {
   grid-template-columns: 1fr;
   gap: 1rem;
  }
`

export default SectionOne;
