import Avatar1 from "../../../../public/image/page/team/team1.png"
import Avatar2 from "../../../../public/image/page/team/team2.png"
import Avatar3 from "../../../../public/image/page/team/team3.png"
import Avatar4 from "../../../../public/image/page/team/team4.png"
import Avatar5 from "../../../../public/image/page/team/team5.png"
import Avatar6 from "../../../../public/image/page/team/team6.png"

// import Color from "../../../../public/image/page/color.png"

import Image from "next/image";

const TeamMates = ()=>{
    return(
        <div>
            <div className="surface-section px-4 md:px-6 lg:px-8 mt-8">
                <div className="text-center font-bold text-900 text-5xl mb-3">Our Team</div>

                <p className="mt-0 mb-4 line-height-3 text-center mx-auto mt-5 mb-8"
                   style={{maxWidth: '630px'}}>
                    Helping companies & individuals implement key solutions for their target markets.
                    We boost their ability to create products.
                    Our business model saves clients time and money. Do not reinvent the wheel.
                </p>

                <div className="grid text-center lg:p-8 lg:m-0 m-5">
                    <div className="lg:col-12 md:col-4 lg:col-3 xl:col-4">
                        <Image src={Avatar2} width={100} height={100} alt="avatar"
                               className="mb-3 border-circle w-7rem h-7rem"/>
                        <div className="text-md text-900 font-bold mb-2">Alex Okoji</div>
                        <div className="text-sm mb-5">Head of Development</div>
                    </div>
                    <div className="lg:col-12  md:col-4 lg:col-3 xl:col-4">
                        <Image src={Avatar1} width={100} height={100} alt="avatar"
                               className="mb-3 border-circle w-7rem h-7rem"/>
                        <div className="text-md text-900 font-bold mb-2">Emmanuel Okpani</div>
                        <div className="text-sm mb-5">Head of Operations</div>
                    </div>
                    <div className="lg:col-12  md:col-4 lg:col-3 xl:col-4">
                        <Image src={Avatar3} width={100} height={100} alt="avatar"
                               className="mb-3 border-circle w-7rem h-7rem"/>
                        <div className="text-md text-900 font-bold mb-2">Chinecherem N.</div>
                        <div className="text-sm mb-5">Head of Design</div>
                    </div>

                    <div className="lg:col-12  md:col-4 lg:col-3 xl:col-4 lg:mt-5">
                        <Image src={Avatar6} width={100} height={100} alt="avatar"
                               className="mb-3 border-circle w-7rem h-7rem"/>
                        <div className="text-md text-900 font-bold mb-2">Mercy Kuchi</div>
                        <div className="text-sm mb-5">Senior Developer</div>
                    </div>
                    <div className="lg:col-12  md:col-4 lg:col-3 xl:col-4 lg:mt-5">
                        <Image src={Avatar5} width={100} height={100} alt="avatar"
                               className="mb-3 border-circle w-7rem h-7rem"/>
                        <div className="text-md text-900 font-bold mb-2">Emmanuel Okoji</div>
                        <div className="text-sm mb-5">Quality Assurance</div>
                    </div>
                    <div className="lg:col-12  md:col-4 lg:col-3 xl:col-4 lg:mt-5">
                        <Image src={Avatar4} width={100} height={100} alt="avatar"
                               className="mb-3 border-circle w-7rem h-7rem"/>
                        <div className="text-md text-900 font-bold mb-2">Rejoice Ejike</div>
                        <div className="text-sm mb-5">Product Manager</div>
                    </div>


                </div>
            </div>
        </div>
    )
}

export default TeamMates