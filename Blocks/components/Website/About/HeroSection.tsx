import {Button} from "primereact/button";
import H1 from "../../../../public/image/page/about/h1.png"
import H2 from "../../../../public/image/page/about/h2.png"
import H3 from "../../../../public/image/page/about/h3.png"
import H4 from "../../../../public/image/page/about/h4.png"
import H5 from "../../../../public/image/page/about/hero5.svg"
import H6 from "../../../../public/image/page/about/hero6.png"
import H7 from "../../../../public/image/page/about/icon.png"
import Image from "next/image";
import React from "react";


const HeroSection =()=>{
    return(
        <div>

            <div className="relative bg-cover z-1 md:p-2 lg:p-0">
                <div className="p-6 flex flex-column align-items-center relative overflow-hidden bg-no-repeat bg-cover"
                    >
                    <article className="flex flex-column align-items-center bg-no-repeat z-2 mt-4">
                        <h1 className=" text-4xl font-bold mt-0 mb-0">
                            We are helping
                            <span className="text-blue-500 p-2">people</span> build their
                        </h1>

                        <h1 className=" text-4xl font-bold mt-1 mb-0">
                            <span className="text-blue-500 p-2">brands</span>
                            and
                            <span className="text-blue-500 p-2">businesses that matter.</span>

                        </h1>


                        <p className="mt-0 text-900 text-md mb-4 line-height-3 text-center mx-auto mt-5"
                           style={{maxWidth: '600px'}}>
                            Our mission is to provide the tools, strategies, and support needed to turn your vision into reality.
                        </p>

                    </article>

                    <Image src={H7} alt="Image" width={300} height={300}
                           className="block mx-auto mt-6 w-full md:w-auto z-1" style={{marginBottom: "-3rem"}}/>
                    <Image src={H2} alt="Image" width={100} height={100}
                           className="hidden xl:block absolute z-1 xl:z-3" style={{top: "66%", left: "73.5%"}}/>
                    <Image src={H4} alt="Image" width={100} height={100}
                         className="hidden xl:block absolute z-1 xl:z-3" style={{top: "70%", right: "77%"}}/>
                    <Image src={H4} alt="Image" width={150} height={150}
                         className="hidden xl:block absolute z-1 xl:z-3" style={{top: "22%", left: "70%"}}/>
                    <Image src={H3} alt="Image" width={100} height={100}
                         className="hidden xl:block absolute z-1 xl:z-3" style={{top: "35%", left: "19%"}}/>

                </div>
            </div>

        </div>
    )
}

export default HeroSection