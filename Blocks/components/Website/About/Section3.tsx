
import styled from "styled-components";

import reportingImg from "../../../../public/image/website/landing_page/reporting.svg";
import complianceImg from "../../../../public/image/website/landing_page/compliance.svg";
import aiImg from "../../../../public/image/website/landing_page/ai.svg";
import messagingSolutionsImg from "../../../../public/image/website/landing_page/messaging-solutions.svg";
import marketingInterventionImg from "../../../../public/image/website/landing_page/marketing-intervention.svg";
import socialMediaManagementImg from "../../../../public/image/website/landing_page/social-media-management.svg";





import {Header_Top, Header_Top_Span, HR_Line} from "../../../ReusedComponent/Text_Typography";
import Card from "../../../ReusedComponent/Card";
import Card_NSK from "../../../ReusedComponent/Card_NSK";
import {Button} from "primereact/button";
import React from "react";
import {CustomButton} from "../../../ReusedComponent/Styled_Button";
import {useRouter} from "next/navigation";


const SectionThree = () => {
    const router = useRouter();

    const NewRoute = () => {
        router.push('/');
    };

    return (
        <Container className="text-700 text-center justify-content-center">
            <div className="  lg:text-center justify-content-center lg:mb-4">
                <h1 className="text-4xl font-bold mt-0 mb-0">
                    Excited to work  <span className="text-blue-500 p-2">with</span> us ?
                </h1>
                <h1 className="text-4xl font-bold mt-0 mb-3">
                    Lets Connect

                </h1>
            </div>

            <div className="flex justify-content-center lg:mt-7">
                <Button
                    label="Contact Us" iconPos="right"
                    style={{
                        background: "#000000", color: '#ffffff',
                        border: "none", height: "3rem"
                    }}/>
            </div>

        </Container>
    );
};

const Container = styled.div`
    //background: #D9D9D9;
    background: linear-gradient(to bottom, #FFFFFF 70%, rgba(7, 120, 253, 0.5) 100%);
    color: #000;
    padding: 7rem;
    line-height: 174%;
    @media (max-width:760px) {
        padding: 2rem;
    }

`;
const InnerContainer = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;

    gap: 2rem;
    @media (max-width:1000px) {
        grid-template-columns: 1fr 1fr;

    }
    @media (max-width:670px) {
        grid-template-columns: 1fr;
        gap: 1rem;
    }
`
export default SectionThree;
