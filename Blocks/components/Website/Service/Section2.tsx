import {Accordion, AccordionTab} from "primereact/accordion";
import Image from "next/image";
import Icon from "../../../../public/image/page/icon2.png";
import React, {useState} from "react";
import {FaChevronDown, FaChevronUp} from "react-icons/fa";
import AccordionItem from "./AccordionItem";



const data = [
    {
        title:"IT Strategy and Planning",
        text:"We assist our clients in developing comprehensive IT strategies aligned with their business goals,\n" +
            "identifying technology needs, and creating roadmaps for implementation"
    },
    {
        title:"Digital Transformation Consulting",
        text:"We guide clients through digital transformation journeys, helping them leverage technology to\n" +
            "optimize operations, enhance customer experiences, and gain a competitive edge."
    },
    {
        title:"Enterprise Architecture Consulting",
        text:"We design and implement enterprise architecture solutions to ensure IT infrastructure\n" +
            "aligns with business objectives and facilitates scalability and agility."
    },
    {
        title:"Cybersecurity Consulting",
        text:"Conduct security assessments, develop cybersecurity strategies, and implement robust security measures to protect client data and systems."
    },
    {
        title:"IT Infrastructure Management Consulting",
        text:"We advise clients on optimizing and managing their IT infrastructure, ensuring efficiency, security, and compliance.."
    },
    {
        title:"AI Consulting",
        text:"e guide you on your AI journey, from readiness assessment to deploying advanced data analytics,\n" +
            "big data solutions,and user-friendly business intelligence dashboards."
    },
    {
        title:"Data Management and Analytics",
        text:"We help you harness the power of your data to gain valuable insights and make\n" +
            "data-driven decisions."
    },


];





const Section2 =()=>{
    const [curOpen, setCurOpen] = useState(null);
    const [allOpen, setAllOpen] = useState(false);


    const handleToggleAll = () => {
        setAllOpen(!allOpen);
        if (!allOpen) {
            setCurOpen(null);
        }
    };


    return(
        <div>

            <div className="surface-section text-800 px-4 md:px-6">
                <div className='flex justify-content-center mb-4'>
                    <Image src={Icon} alt="icon" width={100} height={100}/>
                </div>

                <div className="grid">
                    <div className="col-12 md:col-12 md:px-5">
                        <div className="flex shadow-1 py-3 px-4 justify-content-between">
                            <span className=" block font-bold text-900 text-3xl "> IT Consulting </span>
                            <span className="mt-3 text-lg block cursor-pointer" style={{color: " #0778FD"}}
                                  onClick={handleToggleAll}>
                                {allOpen ? <FaChevronUp/> : <FaChevronDown/>}
                            </span>
                        </div>
                    </div>
                    {allOpen && (
                        <div className="col-12 md:col-12 md:px-5">
                            {data?.map((el, num) => (
                                <AccordionItem
                                    title={el.title}
                                    setCurOpen={setCurOpen}
                                    curOpen={curOpen}
                                    key={num}
                                    num={num}
                                >
                                    {el.text}
                                </AccordionItem>
                            ))}
                        </div>
                    )}

                </div>
            </div>

        </div>
    )
}

export default Section2