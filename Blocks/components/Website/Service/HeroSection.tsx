import Icon from "../../../../public/image/page/🦆 icon _ad product_.png"
import Image from "next/image";
import React from "react";


const HeroSection =()=>{
    return(
        <div>

            <div className="bg-cover z-1">
                <div className="p-4 mt-6 flex flex-column align-items-center relative overflow-hidden bg-no-repeat bg-cover">
                    <div className="flex flex-column lg:align-items-center bg-no-repeat z-2 mt-4">
                        <h1 className="text-4xl font-bold mt-0 mb-0">
                            A Great customer experience
                        </h1>
                        <h1 className="text-4xl font-bold mt-0 mb-0">
                            is<span className="text-blue-500 p-2">Our Priority</span>
                        </h1>
                            <p className="mt-0 mb-4 text-900 line-height-3 lg:text-center mx-auto mt-5"
                               style={{maxWidth: '600px'}}>
                                We create business and innovative for our clients all across the globe.
                                Being very passionate about solving problems in our immediate community ,
                                we love creating digital products that can a solve a variety of problems.
                            </p>

                    </div>



                </div>
            </div>

        </div>
    )
}

export default HeroSection