export const formatDateTime = (timestamp: string) => {
    const updatedAtDate = new Date(timestamp);

    // Format date as YYYY-MM-DD
    const dateOptions: Intl.DateTimeFormatOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
    const date = updatedAtDate.toLocaleDateString('en-CA', dateOptions);

    // Format time as HH:MM:SS
    const timeOptions: Intl.DateTimeFormatOptions = { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false };
    const time = updatedAtDate.toLocaleTimeString('en-GB', timeOptions);

    // Combine date and time
    return `${date} ${time}`;
};
