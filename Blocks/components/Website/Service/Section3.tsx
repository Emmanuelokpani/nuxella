import {Accordion, AccordionTab} from "primereact/accordion";
import Image from "next/image";
import Icon from "../../../../public/image/page/icon3.png";
import React, {useState} from "react";
import {FaChevronDown, FaChevronUp} from "react-icons/fa";
import AccordionItem from "./AccordionItem";


const data = [
    {
        title:"Digital Marketing",
        text:"We develop and execute data-driven digital marketing campaigns (SEO, SEM, conten marketing,\n" +
            "social media marketing) to reach target audiences and achieve marketing objectives."
    },
    {
        title:"Content Management System (CMS) Implementation",
        text:"Implement and manage CMS platforms to empower clients to easily create, edit, and  publish content."
    },
    {
        title:"New Media Management",
        text:"We manage digital and social media presence, create engaging content,\n" +
            "and build online communities to enhance brand awareness and customer engagement."
    },
    {
        title:"Managed IT Services",
        text:"Manage and maintain clients IT infrastructure on an ongoing basis, ensuring optimal \n" +
            "performance, security, and user support."
    },
    {
        title:"Project Management",
        text:"We deliver project management services that encompass the entire project lifecycle,\n" +
            "from strategic planning and execution to agile methodologies."
    },
    {
        title:"User Experience (UX) and Design",
        text:"We prioritize user experience (UX) and design across all digital products. Our\n" +
            "services include customer journey mapping and optimization, prototyping, and usability testing."
    },
    {
        title:"Training and Capacity Building",
        text:"We offer a range of training and capacity-building programs to equip your IT teams\n" +
            "with the latest technical skills"
    },

];




const Section3 =()=>{
    const [curOpen, setCurOpen] = useState(null);
    const [allOpen, setAllOpen] = useState(false);


    const handleToggleAll = () => {
        setAllOpen(!allOpen);
        if (!allOpen) {
            setCurOpen(null);
        }
    };

    return(
        <div>
            <div className="surface-section text-800 px-4 py-4  md:px-6">
                <div className='flex justify-content-center mb-4'>
                    <Image src={Icon} alt="icon" width={100} height={100}/>
                </div>
                <div className="grid">
                    <div className="col-12 md:col-12 md:px-5">
                        <div className="flex shadow-1 py-3 px-4 justify-content-between">
                            <span className=" block font-bold text-900 text-3xl"> Digital Services</span>
                            <span className="mt-3 text-lg block cursor-pointer" style={{color: " #0778FD"}}
                                  onClick={handleToggleAll}>
                                {allOpen ? <FaChevronUp/> : <FaChevronDown/>}
                            </span>
                        </div>
                    </div>
                    {allOpen && (
                        <div className="col-12 md:col-12 md:px-5">
                            {data?.map((el, num) => (
                                <AccordionItem
                                    title={el.title}
                                    setCurOpen={setCurOpen}
                                    curOpen={curOpen}
                                    key={num}
                                    num={num}
                                >
                                    {el.text}
                                </AccordionItem>
                            ))}
                        </div>
                    )}

                </div>
            </div>

        </div>
    )
}

export default Section3