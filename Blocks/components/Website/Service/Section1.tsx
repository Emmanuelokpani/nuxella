import {Accordion, AccordionTab} from "primereact/accordion";
import Image from "next/image";

import Icon from "../../../../public/image/page/icon1.png";
import React, {useState} from "react";
import styled from "styled-components";
import AccordionItem from "./AccordionItem";
import {FaChevronDown, FaChevronUp} from "react-icons/fa";


const data = [
    {
        title: "AI-powered and Automation Solutions",
        text: "We develop and implement AI-powered solutions like chatbots, machine learning models, and intelligent automation tools to streamline processes and enhance decision-making."
    },
    {
        title:"Custom Software Development",
        text:" We design and develop bespoke software applications tailored to address specific client needs and industry challenges."
    },
    {
        title:"Cloud-based Solutions",
        text:"We develop and deploy cloud-based software solutions that are scalable, secure, and accessible"
    },
    {
        title:"Enterprise Mobility Solutions",
        text:"We create mobile applications for businesses to improve operational efficiency, communication, and customer engagement"
    },
    {
        title:"Data Management and Analytics Solutions",
        text:"Develop, implement and manage data management platforms and analytics tools to help  businesses collect,\n" +
            "store, analyze, and leverage data for informed decision-making"
    },
    {
        title:"Blockchain Solutions",
        text:"We develop secure and transparent blockchain-based systems, smart contracts,\n" +
            "and decentralized applications (dApps) for various use cases."
    },
    {
        title:"Mobile and Web Applications",
        text:"We build progressive web apps (PWAs) for seamless cross-platform experiences and\n" +
            "AI-powered e-commerce platforms for personalized shopping experiences."
    },

];


const Section1 =( )=>{
    const [curOpen, setCurOpen] = useState(null);
    const [allOpen, setAllOpen] = useState(false);

    const handleToggleAll = () => {
        setAllOpen(!allOpen);
        if (!allOpen) {
            setCurOpen(null);
        }
    };


    return(
        <div>
            <div className="surface-section text-800 px-4 py-4 md:px-6">
                <div className='flex justify-content-center mb-4'>
                    <Image src={Icon} alt="icon" width={100} height={100}/>
                </div>



                <div className="grid">
                    <div className="col-12 md:col-12 md:px-5 ">
                        <div className="flex shadow-1 py-3 px-4 justify-content-between">
                            <span className=" block font-bold text-900 text-3xl "> Digital Products</span>
                            <span className="mt-3 text-lg block cursor-pointer" style={{color:" #0778FD"}} onClick={handleToggleAll}>
                                {allOpen ? <FaChevronUp/> : <FaChevronDown/>}
                            </span>
                        </div>
                    </div>
                    {allOpen && (
                        <div className="col-12 md:col-12 md:px-5">
                            {data?.map((el, num) => (
                                <AccordionItem
                                    title={el.title}
                                    setCurOpen={setCurOpen}
                                    curOpen={curOpen}
                                    key={num}
                                    num={num}
                                >
                                    {el.text}
                                </AccordionItem>
                            ))}
                        </div>
                    )}

                </div>
            </div>

        </div>
    )
}


export default Section1