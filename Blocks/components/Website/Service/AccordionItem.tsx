import styled from "styled-components";
import {FaMinus, FaPlus} from "react-icons/fa";


const AccordionItem = ({ title, children, setCurOpen, curOpen, num }) => {
    const isOpen = num === curOpen;

    const handleToggle = () => {
        setCurOpen(isOpen ? null : num);
    };

    return (
        <Header  onClick={handleToggle}>
            <Title className={isOpen ? 'open' : ''}>
                <h5>{title}</h5>
                <Icon>{isOpen ? <FaMinus /> : <FaPlus />}</Icon>
            </Title>
            {isOpen && <Content>{children}</Content>}
        </Header>
    );
};

const Header = styled.div`
  display: flex;
  //box-shadow: 0 5px 10px rgba(0, 0, 0, 0.5);
  background-color: #ffffff;
  margin-bottom: 1.2rem;
  cursor: pointer;
  //padding: 1rem 2rem;
  position: relative;
  flex-direction: column;
    border: 1px solid #0778FD;
    
 
`;

const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: #212a2c;
   border: 0 solid #0778FD;
   padding: 0.6rem 2rem;
    h5{
        padding-top: 1rem;
        font-size: 15px;
        font-weight: 600;
    }
    &.open {
        border: 1px solid #0778FD;
    }
`;

const Icon = styled.p`
  font-size: 1rem;
  position: absolute;
  right: 20px;
  top: 20px;
    color: #0778FD;
`;

const Content = styled.div`
    color: #212a2c;
    padding: 2rem 2rem;
`;



export default AccordionItem