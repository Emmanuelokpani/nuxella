import { InputText } from "primereact/inputtext";
import * as Yup from 'yup';
import { Button } from "primereact/button";
import Logo from "../../../../public/image/nuxella.svg"
import { InputTextarea } from "primereact/inputtextarea";
import Image from "next/image";
import React, { useRef, useState } from "react";
import { Calendar } from "primereact/calendar";
import { Dropdown } from "primereact/dropdown";
import { useFormik } from "formik";
import { Toast } from "primereact/toast";
import { formatDateTime } from "./TimeFormatter";


const timeZones = [
    { name: 'Eastern Standard Time (EST)', code: 'EST' },
    { name: 'Central European Time (CET)', code: 'CET' },
    { name: 'Greenwich Mean Time (GMT)', code: 'GMT' },
    { name: 'Turkey Time (TRT)', code: 'TRT' },
    { name: 'Central European Summer Time (CEST)', code: 'CEST' },
    { name: 'West Africa Time (WAT)', code: 'WAT' }
];

const validationSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    email: Yup.string().email('Invalid email address').required('Email is required'),
    time: Yup.date().required('Date/Time is required'),
    timeZone: Yup.object().shape({
        name: Yup.string().required('Time Zone is required'),
        code: Yup.string().required('Time Zone is required')
    }).nullable().required('Time Zone is required'),
    description: Yup.string().required('Description is required'),
});

const FormData = () => {
    const [datetime12h, setDateTime12h] = useState(null);
    const [selectedTimeZone, setSelectedTimeZone] = useState(null);
    const [loading, setLoading] = useState(false);
    const toast = useRef<Toast | null>(null);

    const initialValues = {
        name: '',
        email: '',
        time: '',
        timeZone: null,
        description: '',
    };

    const onSubmit = async (values: any, { setSubmitting }: any) => {
        const { name, email, time, timeZone, description } = values;
        if (!name || !email || !time || !timeZone || !description) {
            toast.current?.show({
                severity: 'info',
                summary: 'Validation Error',
                detail: "Please fill in all required fields to schedule a meeting",
                life: 3000,
            });
            return;
        }
        const formattedTime = formatDateTime(time);
        console.log(timeZone.code, "code")
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name,
                email,
                time: formattedTime,
                timeZone,
                description
            })
        };

        setLoading(true);
        try {
            const url = `https://nuxalle-70782-default-rtdb.firebaseio.com/messages.json`;
            const res = await fetch(url, options);
            if (res.ok) {
                console.log("Data saved successfully!");
                toast.current?.show({
                    severity: 'info',
                    summary: 'Success',
                    detail: "Meeting scheduled successfully",
                    life: 3000,
                });
            } else {
                console.error("Error saving data");
                toast.current?.show({
                    severity: 'error',
                    summary: 'Error',
                    detail: "Error scheduling meeting",
                    life: 3000,
                });
            }
            setLoading(false);
            formik.resetForm();
        } catch (error) {
            console.error('Error calling upload API:', error);
            setLoading(false);
            toast.current?.show({
                severity: 'error',
                summary: 'Error',
                detail: "Error scheduling meeting",
                life: 3000,
            });
        }
    };

    const formik = useFormik({
        initialValues,
        validationSchema,
        onSubmit
    });

    return (
        <div>
            <Toast ref={toast} />
            <div className="px-4 py-8 md:px-6 lg:px-8">
                <div className="grid lg:p-8">
                    <div className="hidden md:block col-12 md:col-6 mt-7 px-0 py-4 lg:p-7 bg-gray-900 shadow-2">
                        <Image src={Logo} alt="logo" className='w-full' width={140} />
                    </div>

                    <div className="col-12 md:col-6 lg:mt-4 p-4 lg:p-6">
                        <div className="">
                            <label htmlFor="name" className="block text-900 font-bold mb-2">Name</label>
                            <InputText
                                id="name" name="name"
                                value={formik.values.name}
                                onChange={formik.handleChange}
                                type="text" placeholder="Name"
                                className={`w-full py-2`}
                            />
                            {formik.touched.name && formik.errors.name && <small className="p-error">{formik.errors.name.toString()}</small>}
                        </div>

                        <div className="mt-4">
                            <label htmlFor="email" className="block text-900 font-bold mb-2">Email Address</label>
                            <InputText
                                id="email" name="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                type="email" placeholder="Email address"
                                className={`w-full py-2`}
                            />
                            {formik.touched.email && formik.errors.email && <small className="p-error">{formik.errors.email.toString()}</small>}
                        </div>

                        <div className="grid mt-4">
                            <div className="flex-auto md:col-5">
                                <label htmlFor="calendar-12h" className="font-bold block">
                                    Date / Time
                                </label>
                                <Calendar
                                    id="time" name="time"
                                    value={formik.values.time}
                                    onChange={formik.handleChange}
                                    showTime hourFormat="12"
                                    placeholder="Time"
                                    className="w-full py-2"
                                />
                                {formik.touched.time && formik.errors.time && <small className="p-error">{formik.errors.time.toString()}</small>}
                            </div>
                            <div className="flex-auto md:col-6 pt-2">
                                <label htmlFor="calendar-12h" className="font-bold block mb-2">Time Zone</label>
                                <Dropdown
                                    name="timeZone"
                                    id="timeZone"
                                    optionLabel="name"
                                    options={timeZones}
                                    value={formik.values.timeZone}
                                    onChange={formik.handleChange}
                                    placeholder="Time Zone"
                                    className={`w-22rem md:w-14rem mt-0 p-0 mb-2`}
                                />
                                {formik.touched.timeZone && formik.errors.timeZone && <small className="p-error">{formik.errors.timeZone.toString()}</small>}
                            </div>
                        </div>

                        <div className="mt-4">
                            <label htmlFor="description" className="block text-900 font-medium mb-3">
                                Please share the topics you would like to discuss. That
                                will help us prepare for our meeting.
                            </label>

                            <InputTextarea
                                id="description" name="description"
                                value={formik.values.description}
                                onChange={formik.handleChange}
                                placeholder="Description"
                                rows={6} autoResize
                                className={`w-full p-3`}
                            />
                            {formik.touched.description && formik.errors.description && <small className="p-error">{formik.errors.description.toString()}</small>}
                        </div>

                        <Button
                            type="button"
                            onClick={() => formik.handleSubmit()}
                            label={loading ? 'Please Wait...' : 'Schedule Meeting'}
                            icon={loading ? 'pi pi-spin pi-spinner' : 'pi pi-calendar-plus'}
                            className="w-full mt-6 bg-blue-500 border-none text-sm text-white py-3 font-medium"
                            disabled={loading}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FormData
