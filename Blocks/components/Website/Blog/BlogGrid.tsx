import Icon6 from "../../../../public/image/page/blog/icon6.png"
import Image from 'next/image';
import React from "react";
import styled from "styled-components";


const BlogGrid =()=>{
    return (
        <div>
            <Container id="hero-section-container">
                <div className=" flex flex-wrap lg:p-8">
                    <div className="w-12 lg:w-6">
                        <Image src={Icon6} alt="blog" className="blogImage" width={500} height={300}/>
                    </div>
                    <div className="w-12 lg:w-5  overflow-hidden">
                        <span className="text-blue-300 font-medium text-sm">Chinecherem Nduka • 1 Jan 2023</span>
                        <p className="text-900 font-bold text-lg  mt-3 mb-2">Grid system for better Design User
                            Interface</p>
                        <p className="text-900 text-sm">
                        A grid system is a design tool used to arrange content
                            on a webpage. It is a series of vertical and horizontal
                            lines that create a matrix of intersecting points,
                            which can be used to align and organize page elements.
                            Grid systems are used to create a consistent look and feel across a website, and can help
                            to make the layout more visually appealing and easier to navigate.
                        </p>

                        <p className="text-900 text-sm mt-3">
                        A grid system is a design tool used to arrange content
                            on a webpage. It is a series of vertical and horizontal
                            lines that create a matrix of intersecting points,
                            which can be used to align and organize page elements.
                            Grid systems are used to create a consistent look and feel across a website, and can help
                            to make the layout more visually appealing and easier to navigate.
                        </p>

                        <div className="flex justify-content-start mr-0 ml-0 gap-3 mt-4">
                            <span className="bg-blue-100  text-sm  px-3 p-1 border-round  ">Design</span>
                            <span className="bg-pink-100  text-sm px-3  p-1 border-round">Interface</span>
                            <span className="bg-green-100  text-sm px-3  p-1 border-round">Design</span>

                        </div>

                    </div>
                </div>
            </Container>

        </div>

    )
}

export default BlogGrid


const Container = styled.div`
    width: 100%;
    //position: relative;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    gap: 4rem;
    flex-direction: column;


    @media (max-width: 658px) {
        padding: 1rem;

        .blogImage {
            height: 40vh;
            width: 100%;
        }
    }

`;
