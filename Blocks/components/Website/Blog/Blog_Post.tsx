import {Ripple} from "primereact/ripple";
import Image from 'next/image';

import Icon1 from "../../../../public/image/page/blog/icon1.png"
import Icon2 from "../../../../public/image/page/blog/icon2.png"
import Icon3 from "../../../../public/image/page/blog/icon3.png"
import Icon4 from "../../../../public/image/page/blog/icon4.png"
import Icon5 from "../../../../public/image/page/blog/icon5.png"
import Icon6 from "../../../../public/image/page/blog/icon6.png"


const Blog_Post =()=>{
    return (
        <div>

            <div className="surface-section px-4 mb-8 md:px-6 lg:px-8">
                <p className="mt-0 p-0 mb-5  font-bold text-2xl">All blog posts</p>

                <div className="grid -ml-3 -mr-3 p-3">
                    <div className="col-12 md:col-6 lg:col-4 mb-3 lg:mb-0">
                        <div className="p-2">
                            <div className="relative">
                                <Image src={Icon1} width={100} height={250} alt="blog"
                                       className="w-full mb-2"/>
                            </div>
                            <span className="text-blue-300 font-medium text-sm ">Rejoice Ejike • 1 Jan 2023</span>
                            <p className="text-900 font-bold text-lg  mt-3 mb-2">Bill Walsh leadership lessons</p>
                            <span className="text-900 text-sm ">Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?</span>

                            <div className="flex justify-content-start mr-0 ml-0 gap-3 mt-3">
                                <span className="bg-blue-100  text-sm  px-3 p-1 border-round  ">Research</span>
                                <span className="bg-pink-100  text-sm px-3  p-1 border-round">Management</span>
                                <span className="bg-green-100  text-sm px-3  p-1 border-round">Design</span>

                            </div>
                        </div>
                    </div>
                    <div className="col-12 md:col-6 lg:col-4 mb-3 lg:mb-0">
                        <div className="p-2">
                            <div className="relative">
                                <Image src={Icon2} width={100} height={250} alt="blog"
                                       className="w-full mb-2"/>
                            </div>
                            <span className="text-blue-300 font-medium text-sm ">Rejoice Ejike • 1 Jan 2023</span>
                            <p className="text-900 font-bold text-lg  mt-3 mb-2">Bill Walsh leadership lessons</p>
                            <span className="text-900 text-sm ">Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?</span>

                            <div className="flex justify-content-start mr-0 ml-0 gap-3 mt-3">
                                <span className="bg-blue-100  text-sm  px-3 p-1 border-round  ">Research</span>
                                <span className="bg-pink-100  text-sm px-3  p-1 border-round">Management</span>
                                <span className="bg-green-100  text-sm px-3  p-1 border-round">Design</span>

                            </div>
                        </div>
                    </div>
                    <div className="col-12 md:col-6 lg:col-4 mb-3 lg:mb-0">
                        <div className="p-2">
                            <div className="relative">
                                <Image src={Icon3} width={100} height={250} alt="blog"
                                       className="w-full mb-2"/>
                            </div>
                            <span className="text-blue-300 font-medium text-sm ">Rejoice Ejike • 1 Jan 2023</span>
                            <p className="text-900 font-bold text-lg  mt-3 mb-2">Bill Walsh leadership lessons</p>
                            <span className="text-900 text-sm ">Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?</span>

                            <div className="flex justify-content-start mr-0 ml-0 gap-3 mt-3">
                                <span className="bg-blue-100  text-sm  px-3 p-1 border-round  ">Research</span>
                                <span className="bg-pink-100  text-sm px-3  p-1 border-round">Management</span>
                                <span className="bg-green-100  text-sm px-3  p-1 border-round">Design</span>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    )
}


export default Blog_Post