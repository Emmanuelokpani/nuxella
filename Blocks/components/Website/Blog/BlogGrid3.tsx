import Image from 'next/image';
import React from "react";
import styled from "styled-components";
import Icon1 from "../../../../public/image/page/blog/icon1.png";
import Icon4 from "../../../../public/image/page/blog/icon4.png"
import Icon5 from "../../../../public/image/page/blog/icon5.png"
import Icon6 from "../../../../public/image/page/blog/icon6.png"


const BlogGrid3 =()=>{
    return (
        <div>
            <Container id="hero-section-container">

                <div className=" flex flex-wrap lg:p-8">
                    <div className="w-12 lg:w-6">
                        <p className="mt-0  mb-0 font-bold text-2xl">Recent blog posts</p>

                        <div className="p-3">
                            <div className="relative">
                                <Image src={Icon4} width={100} height={300} alt="blog"
                                       className="w-full mb-2"/>
                            </div>
                            <span className="text-blue-300 font-medium text-sm ">Rejoice Ejike • 1 Jan 2023</span>
                            <p className="text-900 font-bold text-lg  mt-3 mb-2">Bill Walsh leadership lessons</p>
                            <span className="text-900 text-sm ">Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?</span>

                            <div className="flex justify-content-start mr-0 ml-0 gap-3 mt-2">
                                <span className="bg-blue-100  text-sm  px-3 p-1 border-round  ">Research</span>
                                <span className="bg-pink-100  text-sm px-3  p-1 border-round">Management</span>
                                <span className="bg-green-100  text-sm px-3  p-1 border-round">Design</span>

                            </div>
                        </div>
                    </div>

                    <div className="w-12 lg:w-6 mt-6 overflow-hidden">
                    <div className="flex flex-wrap ">
                            <div className="w-12 lg:w-6">
                                <Image src={Icon5} alt="blog" className="blogImage" width={270} height={160}/>
                            </div>
                            <div className="w-12 lg:w-6  overflow-hidden">
                                <span
                                    className="text-blue-300 font-medium text-sm">Chinecherem Nduka • 1 Jan 2023</span>
                                <p className="text-900 font-bold text-lg  mt-2 mb-2">Grid system for better Design</p>

                                <p className="text-900 text-sm mt-2">
                                    A grid system is a design tool used to arrange content
                                    on a webpage. It is a series of vertical and horizontal
                                    lines that create a matrix of intersecting points,
                                    which can be used to align and organize page elements.
                                </p>

                                <div className="flex justify-content-start mr-0 ml-0 gap-3 ">
                                    <span className="bg-blue-100  text-sm  px-3 p-1 border-round  ">Design</span>
                                    <span className="bg-pink-100  text-sm px-3  p-1 border-round">Interface</span>
                                    <span className="bg-green-100  text-sm px-3  p-1 border-round">Design</span>

                                </div>

                            </div>
                        </div>
                        <div className="flex flex-wrap mt-5">
                            <div className="w-12 lg:w-6">
                                <Image src={Icon6} alt="blog" className="blogImage" width={270} height={160}/>
                            </div>
                            <div className="w-12 lg:w-6  overflow-hidden">
                                <span
                                    className="text-blue-300 font-medium text-sm">Chinecherem Nduka • 1 Jan 2023</span>
                                <p className="text-900 font-bold text-lg  mt-2 mb-2">Grid system for better Design</p>

                                <p className="text-900 text-sm mt-2">
                                    A grid system is a design tool used to arrange content
                                    on a webpage. It is a series of vertical and horizontal
                                    lines that create a matrix of intersecting points,
                                    which can be used to align and organize page elements.
                                </p>

                                <div className="flex justify-content-start mr-0 ml-0 gap-2 ">
                                    <span className="bg-blue-100  text-sm  px-3 p-1 border-round  ">Design</span>
                                    <span className="bg-pink-100  text-sm px-3  p-1 border-round">Interface</span>
                                    <span className="bg-green-100  text-sm px-3  p-1 border-round">Design</span>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </Container>

        </div>

    )
}

export default BlogGrid3


const Container = styled.div`
    width: 100%;
    //position: relative;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    gap: 4rem;
    flex-direction: column;


    @media (max-width: 658px) {
        padding: 1rem;

        .blogImage {
            height: 40vh;
            width: 100%;
        }
    }

`;
