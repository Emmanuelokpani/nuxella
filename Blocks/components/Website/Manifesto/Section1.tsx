import styled from 'styled-components';
import { useRouter } from 'next/navigation';
import Image from "next/image";
import HeroBackground from '../../../../public/image/website/landing_page/heroBackground.png';
import Hero from '../../../../public/image/page/Lingkaran.png';
import React from 'react';



const SectionOne = () => {
    const router = useRouter();

    const NewRoute = () => {
        router.push('/register');
        console.log('clicked');
    };

    return (

            <Container id="hero-section-container ">
                <div className=" lg:mb-5 flex flex-wrap ">
                    <div className="lg:w-10 lg:p-8">
                        <h1 className="text-6xl font-bold text-white mt-0 mb-3">
                            Our Value Proposition
                        </h1>
                        <p className="lg:text-2xl text-white mt-0 mb-5">
                            Nuxalle stands out by <span className="text-blue-500"> inventing the future,</span> offering
                            solutions that redefine industries,
                            and providing end-to-end digital services that
                            <span className="text-blue-500"> elevate brands</span> to
                            <span className="text-blue-500"> global prominence.</span>
                        </p>

                        <p className="text-2xl text-white mt-0 mb-5">
                            We have an <span className="text-blue-500"> African-born global </span>
                            perspective, understanding the nuances of both emerging and developed markets
                        </p>

                        <p className="text-2xl text-white mt-0 mb-5">
                            We think <span className="text-blue-500"> outside the box,</span>
                            and approach that leads to developing disruptive solutions not offered by competitors.
                        </p>
                    </div>
                    <div className="lg:w-2">
                        <Image className="hidden md:block" src={Hero} alt="Hero" width={200} height={300}/>
                    </div>
                </div>
            </Container>




    );
};

export default SectionOne;

const Container = styled.div`
    background-color: #000000;
    width: 100%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    flex-direction: column;
    
    @media (max-width: 658px) {
        padding: 2rem;
    }

`;

const OverlayContainer = styled.div`
    background-color: #D9D9D9;
    height: 150px;
    position: relative;
   
    @media (max-width: 992px) {
        display:none
    }
`


const OverlayImageContainer = styled.div`
    position: absolute;
    top: -50px; 
    left: 50%;
    transform: translateX(-50%);
    width: 90%;
    height: 250px;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;


  
`;

