import Image from "next/image";
import M1 from "../../../../public/image/page/blue_Icons/Icon1.png";
import M2 from "../../../../public/image/page/blue_Icons/Icon2.png";
import M3 from "../../../../public/image/page/blue_Icons/Icon3.png";
import M4 from "../../../../public/image/page/blue_Icons/Icon4.png";
import M5 from "../../../../public/image/page/blue_Icons/Icon5.png";
import M6 from "../../../../public/image/page/blue_Icons/Icon6.png";

import React from "react";
import styled from "styled-components";

const SectionThree =()=>{
    return (
        <Container id="hero-section-container " className="p-4">
            <div className="flex flex-wrap ">
                <div className="flex justify-content-center w-12 lg:w-12">
                    <h1 className="text-5xl font-bold text-white mt-8 mb-4">
                        What makes us different?
                    </h1>

                </div>
            </div>

            <div className="grid w-12 mt-6  justify-content-center">
                <div className="col-12  md::col-3 lg:m-3 lg:p-6 xl:col-5 mt-4">
                    <h6 className="text-blue-500 text-3xl font-bold">Comprehensive </h6>
                    <p className="text-white text-md  ">
                        After examining deeply about your business model we come up with a varied and
                        comprehansive range of solutions in a most affordable range
                    </p>
                </div>
                <div className="col-12  md::col-3 lg:m-3 lg:p-6 xl:col-5 mt-4 ">
                    <h6 className="text-blue-500 text-3xl font-bold">Committed timelines </h6>
                    <p className="text-white text-md  ">
                        After examining deeply about your business model we come up with a varied and
                        comprehansive range of solutions in a most affordable range
                    </p>
                </div>
                <div className="col-12 md:col-3 lg:m-3 lg:p-6  xl:col-5 mt-4 ">
                    <h6 className="text-blue-500 text-3xl font-bold">Customized solution </h6>
                    <p className="text-white text-md  ">
                        After examining deeply about your business model we come up with a varied and
                        comprehansive range of solutions in a most affordable range
                    </p>
                </div>
                <div className="col-12 md:col-3 lg:m-3 lg:p-6 xl:col-5 mt-4">
                    <h6 className="text-blue-500 text-3xl font-bold">Quality services </h6>
                    <p className="text-white text-md  mx-auto">
                        After examining deeply about your business model we come up with a varied and
                        comprehansive range of solutions in a most affordable range
                    </p>
                </div>
                <div className="col-12 md:col-3 lg:m-3  lg:p-6 xl:col-5 mt-4">
                    <h6 className="text-blue-500 text-3xl font-bold">Strategic approach </h6>
                    <p className="text-white text-md  ">
                        After examining deeply about your business model we come up with a varied and
                        comprehansive range of solutions in a most affordable range
                    </p>
                </div>
                <div className="col-12 md:col-3  lg:m-3 lg:p-6 xl:col-5 mt-4">
                    <h6 className="text-blue-500 text-3xl font-bold">Flexible and budget </h6>
                    <p className="text-white text-md  ">
                        After examining deeply about your business model we come up with a varied and
                        comprehansive range of solutions in a most affordable range
                    </p>
                </div>
            </div>

        </Container>
    )
}


export default SectionThree


const Container = styled.div`
    padding-bottom:5rem;
    background-color: #000000;
    width: 100%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    //gap: 4rem;
    flex-direction: column;


    @media (max-width: 658px) {
        padding: 1rem;
        //height:65rem;
        .bracelet{
            width:100%;
            //margin-top:2rem
        }
    }

`;

