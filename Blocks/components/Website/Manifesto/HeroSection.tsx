import styled from 'styled-components';
import { useRouter } from 'next/navigation';
import Image from 'next/image';
import M1 from '../../../../public/image/page/m1.png';
import M2 from '../../../../public/image/page/m2.png';
import M3 from '../../../../public/image/page/m3.png';
import M4 from '../../../../public/image/page/m4.png';
import React from 'react';



const HeroSection = () => {
    const router = useRouter();

    const NewRoute = () => {
        router.push('/register');
        console.log('clicked');
    };

    return (

            <Container id="hero-section-container  overflow-hidden ">
                <div className="flex flex-wrap lg:p-8">
                    <div className="w-12 lg:w-12 mt-8">
                        <h2 className="lg:text-5xl font-bold text-white mt-0 mb-0">
                            <span className="lg:text-7xl">We </span> are a propellant fuel for
                        </h2>
                        <h2 className="lg:text-5xl font-bold text-white mt-0 mb-0">
                            your business ready to take
                        </h2>
                        <h2 className="lg:text-5xl font-bold text-white mt-0 ">
                            your Business sky high.
                        </h2>

                    </div>
                </div>
                <div className="grid lg:m-8">
                    <div className="col-12 md:col-12 xl:col-3 p-3">
                        <Image src={M1} alt="bracelet" width={200} height={350}
                               className="mb-3 bracelet"/>
                    </div>
                    <div className="col-12 md:col-6 xl:col-3 p-3" style={{marginTop:"7rem"}}>
                        <Image src={M2} alt="bracelet" width={200} height={350}
                               className=" hidden md:block"/>
                    </div>
                    <div className="col-12 md:col-6 xl:col-3 p-3">
                        <Image src={M3} alt="bracelet" width={200} height={350}
                               className="mb-3  hidden md:block"/>
                    </div>
                    <div className="col-12 md:col-6 xl:col-3 p-3" style={{marginTop:"7rem"}}>
                        <Image src={M4} alt="bracelet" width={200} height={350}
                               className="lg:mb-3  hidden md:block"/>
                    </div>
                </div>

            </Container>


    );
};

export default HeroSection;

const Container = styled.div`
   
    background-color: #000000;
    width: 100%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    //gap: 4rem;
    flex-direction: column;


    @media (max-width: 658px) {
        padding: 1rem;
        height:65rem;
        .bracelet{
            width:100%;
            height:70vh;
            margin-top:2rem
        }
    }

`;






