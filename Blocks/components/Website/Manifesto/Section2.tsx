import Image from "next/image";
import M1 from "../../../../public/image/page/blue_Icons/Icon1.png";
import M2 from "../../../../public/image/page/blue_Icons/Icon2.png";
import M3 from "../../../../public/image/page/blue_Icons/Icon3.png";
import M4 from "../../../../public/image/page/blue_Icons/Icon4.png";
import M5 from "../../../../public/image/page/blue_Icons/Icon5.png";
import M6 from "../../../../public/image/page/blue_Icons/Icon6.png";

import React from "react";
import styled from "styled-components";

const SectionTwo =()=>{
    return (
        <Container id="hero-section-container  overflow-hidden">
            <div className="flex flex-wrap ">
                <div className="flex justify-content-center w-12 lg:w-12">
                    <h1 className="text-5xl font-bold text-white mt-0 mb-0">
                        Our Values
                    </h1>

                </div>
            </div>

            <div className="grid lg:m-8 lg:pl-8 overflow-hidden">
                <div className="col-12 lg:p-0 p-3 md::col-3 xl:col-4 ">
                    <Image src={M1} alt="bracelet" width={210} height={300}
                           className=" bracelet hidden md:block"/>
                </div>
                <div className="col-12 lg:p-0 p-3  md::col-3 xl:col-4 " >
                    <Image src={M2} alt="bracelet" width={210} height={300}
                           className=" bracelet"/>
                </div>
                <div className="col-12 lg:p-0 p-3  md:col-3 xl:col-4 ">
                    <Image src={M3} alt="bracelet" width={210} height={300}
                           className="  bracelet"/>
                </div>
                <div className="col-12 lg:p-0 p-3 md:col-3 xl:col-4 lg:mt-4" >
                    <Image src={M4} alt="bracelet" width={210} height={300}
                           className="  bracelet hidden md:block"/>
                </div>
                <div className="col-12 lg:p-0 p-3  md:col-3 xl:col-4 lg:mt-4" >
                    <Image src={M5} alt="bracelet" width={210} height={300}
                           className=" bracelet"/>
                </div>
                <div className="col-12 lg:p-0 p-3  md:col-3 xl:col-4 lg:mt-4" >
                    <Image src={M6} alt="bracelet" width={210} height={300}
                           className="  bracelet"/>
                </div>
            </div>
        </Container>
    )
}


export default SectionTwo


const Container = styled.div`
   
    background-color: #000000;
    width: 100%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    gap: 4rem;
    flex-direction: column;


    @media (max-width: 658px) {
        padding: 1rem;
        //height:65rem;
        .bracelet{
            width:100%;
            height:60vh;
            margin-top:1rem
        }
    }

`;

