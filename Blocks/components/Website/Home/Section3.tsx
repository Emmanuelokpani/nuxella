
import styled from "styled-components";
import aiImg from "../../../../public/image/page/Lingkaran2.png";
import Card from "../../../ReusedComponent/Card";
// import Card_NSK from "../../../ReusedComponent/Card_NSK";
import {Button} from "primereact/button";
import React from "react";
import Image from "next/image";


interface CardProps {
    subTitle?: string;
    title: string;
    content: string;
    icon?: any;
    index:number
}
const Items = [
    {
        title: "Businesses",
        content: `We partner with businesses and brands of all sizes across various industries. Whether you're a startup looking to make a mark or an established company seeking to innovate, we provide the digital solutions and expertise needed to achieve your goals. Our services help
         businesses increase efficiency, enhance customer engagement, and drive growth.`,

    },
    {
        title: "Organizations",
        content: `We collaborate with a diverse range of organizations, including NGOs, nonprofits, and community groups. Whether it's through developing custom software, offering IT consulting, or implementing digital strategies, our goal is to empower these organizations
         with technology that enhances their operations and maximizes their impact.`,

    },
    {
        title: "Institutions",
        content: `Our solutions are tailored to meet the unique needs of each institution, driving progress and discovery. We provide the tools and support needed to advance their objectives, whether it’s through developing research platforms,
         enhancing digital learning experiences, or collaborating on cutting-edge research initiatives. `,

    },

];

function Card_NSK(props: CardProps) {
    const {subTitle, title, content, icon, index} = props;

    return (
        <CardContainer className="grid" key={index}>
            <div className="col-12 md:col-12 lg:p-1">
                <div className="flex  justify-content-center mb-3">
                    <Image src={icon} className="mr-3" alt=""/>
                </div>
                <div className="flex justify-content-center surface-border">
                    <span className="text-sm text-900 font-bold">{subTitle}</span>
                </div>
                <div className="flex  surface-border mb-2 pb-2">
                    <span className="text-3xl text-900 font-bold">{title}</span>
                </div>
                <TextDiv className="text-md text-900 mt-4">{content}</TextDiv>
            </div>
        </CardContainer>
    );
}



const SectionThree = () => {
    return (
        <Container className="relative text-700 lg:text-center justify-content-center ">
            <div className="absolute top-0 bottom-2 right-0  hidden md:block">
                <Image className="hidden md:block" src={aiImg} alt="Hero" width={130} height={250}/>
            </div>

            <div className=" lg:text-center lg:justify-content-center lg:mb-8">
                <h1 className="text-4xl font-bold mt-0 mb-0">
                    We work with all the <span className="text-blue-500">genius</span>
                </h1>
                <h1 className="text-4xl font-bold mt-0 mb-3">
                    seeking expression

                </h1>

            </div>


            <InnerContainer>
                {Items.map((item, index) => (
                    <Card_NSK index={index} key={index} title={item.title} content={item.content}/>
                ))}
            </InnerContainer>


            <div className="flex justify-content-center mt-8 ">
                <Button
                    label="Lets Talk" icon="pi pi-arrow-right font-bold text-lg" iconPos="right"
                    style={{
                        background: "#0778FD", color: '#ffffff',
                        border: "1px solid #0778FD", height: "3rem"
                    }}/>
            </div>
        </Container>
    );
};

const Container = styled.div`
    //background: #D9D9D9;
    background: linear-gradient(to bottom, #FFFFFF 70%, rgba(7, 120, 253, 0.5) 100%);
    color: #000;
    padding: 8rem;
    line-height: 174%;
    @media (max-width: 760px) {
        padding: 1.5rem;
    }

`;
const InnerContainer = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;

    gap: 2rem;
   
    @media (max-width:670px) {
        grid-template-columns: 1fr;
        gap: 1rem;
    }
`


const CardContainer = styled.div`
    width: 100%;
    margin-bottom: 0 !important;
    display: flex;
    flex-direction: column;
    text-align: start;
    //align-items: center;
    //justify-content: center;

`;

const TextDiv = styled.div`
    color: #6f7070;
    //text-align: center;
`;

export default SectionThree;
