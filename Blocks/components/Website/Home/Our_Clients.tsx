
import React from "react";
import styled from "styled-components";
import {Header_Top, Header_Top_Span, HR_Line} from "../../../ReusedComponent/Text_Typography";
import {portfolioCardContents} from "../LandingPage/data/data";

const Our_Clients =()=> {
  return (
    <Container>
      <div className="text-center">
        <Header_Top label="Our Clients" />
        <HR_Line className="w-3 md:w-2 lg:w-1 mb-3" />
        <Header_Top_Span
          label="We serve companies of all sizes, all around the globe.
          We speak your language"
          className="font-light text-2xl p-2"
          />
      </div>

      <Card>
        {portfolioCardContents.map((item:any, id:any) => (
          <img src={item.icon.src} key={id} />
          ))}
      </Card>
    </Container>
         
  );
}
export default Our_Clients
const Container = styled.div`
  width: 100%;

  padding: 4rem;
  background-color: #f9fcff;
  @media (max-width:760px) {
    padding: 2rem;
  }
`;


const SectionTitle = styled.h2`
  font-weight: 400;
  margin-bottom: 2rem;
  text-align: center;
`;

const SectionSubtitle = styled.p`
  font-size: 2rem;
  font-weight: 300;
  text-align: center;
`;

const Card = styled.div`
 display: grid;
 margin-top: 1rem;
 grid-template-columns: 1fr 1fr 1fr;
 align-items: center;
 justify-content: center;
 gap: 2rem;
 img{
  width: 100%;
  max-width: 200px;
  align-self: center;
  justify-self: center;
 }

`;
