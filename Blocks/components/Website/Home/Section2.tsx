'use client';
import HeroImg from '../../../../public/image/page/section2.png';
import Image from 'next/image';
import styled from 'styled-components';
import { CustomButton } from '../../../ReusedComponent/Styled_Button';
import { useRouter } from 'next/navigation';
import HeroBackground from '../../../../public/image/website/landing_page/heroBackground.png';
import React from "react";
import SectionThree from "./Section3";
import Card, { TextDiv} from "../../../ReusedComponent/Card";
import reportingImg from "../../../../public/image/website/landing_page/reporting.svg";
import complianceImg from "../../../../public/image/website/landing_page/compliance.svg";
import aiImg from "../../../../public/image/website/landing_page/ai.svg";
import messagingSolutionsImg from "../../../../public/image/website/landing_page/messaging-solutions.svg";
import marketingInterventionImg from "../../../../public/image/website/landing_page/marketing-intervention.svg";
import socialMediaManagementImg from "../../../../public/image/website/landing_page/social-media-management.svg";
import {Carousel} from "primereact/carousel";


const Items = [
    {
        title: "Multi-Telco Compliance",
        content: `We meet global texting standards that allows real time interaction & messaging delivery.`,
        icon: complianceImg,
    },

    {
        title: "Excellent Reporting",
        content: `Easy to use & manage direct messaging & two-way messaging communication with customers in a unique way.`,
        icon: messagingSolutionsImg,
    },
    {
        title: "Tailored Digital Strategy & Marketing Intervention",
        content: `Maximize market opportunities for your business through digital marketing technologies. 
              Reach the right audience, drive results & maximize your campaign investments.`,
        icon: marketingInterventionImg,
    },

];






const SectionTwo = () => {
    const router = useRouter();

    const NewRoute = () => {
        router.push('/register');
        console.log('clicked');
    };

    const responsiveOptions = [
        {
            breakpoint: '1400px',
            numVisible: 2,
            numScroll: 1
        },
        {
            breakpoint: '1199px',
            numVisible: 3,
            numScroll: 1
        },
        {
            breakpoint: '767px',
            numVisible: 2,
            numScroll: 1
        },
        {
            breakpoint: '575px',
            numVisible: 1,
            numScroll: 1
        }
    ];

    const Template = (item:any) => {
        return (
            <CardContainer className="card" key={item.index}>
                <div className="flex flex-row  mb-3">
                    <Image src={item.icon} width={30} height={30} className="mr-3" alt="1"/>
                </div>

                <TextDiv className="text-sm">{item.content}</TextDiv>
                <div className="flex flex-row text-start mb-2 mt-4">
                    <span className="text-md font-bold mb-1 ">{item.title}</span>
                </div>
            </CardContainer>
        );
    };


    return (
        <Wrapper>
            <div className="grid lg:p-6">
                <div className="col-12 md:col-7">
                    <div className="lg:p-4 justify-content-center lg:mb-8">
                        <h1 className="text-white text-4xl font-bold mt-0 mb-0">
                            WE approach challenges with a
                            <span className="text-blue-500 p-2">Perspective</span>
                            you will not find anywhere else
                        </h1>

                        <p className="mt-3">
                            At Nuxalle, we are not just a company; we are a gateway to a world of endless possibilities.
                            Our passion for excellence and commitment to pushing the boundaries of what is possible
                            drive everything we do.
                        </p>
                        <p className="mt-4">
                            With a team of creative visionaries and technical wizards, we craft solutions that are not
                            only innovative but also user-centric. Whether you are an individual, a small business, or a
                            large enterprise, we have something extraordinary to offer you.
                        </p>
                    </div>
                </div>
                <div className="col-12 md:col-5">
                    <Image
                        width={400}
                        height={380}
                        src={HeroImg.src}
                        alt="hero-1"
                        className="image md:ml-auto block "
                    />
                </div>
            </div>


            <div className="hidden md:block ">
                <div className="lg:mt-8 text-1xl">
                    <p className="flex justify-content-center text-white ">
                        <span className="pr-2"> Read </span>
                        <span className=" underline ">
                    OUR MANIFESTO</span>
                    </p>
                    <h1 className="text-white text-4xl font-bold mt-0 mb-3 flex justify-content-center lg:mt-6">
                        What people are saying
                    </h1>
                </div>

                <Carousel value={Items} numVisible={2} numScroll={3} className="custom-carousel mt-8 " circular
                          responsiveOptions={responsiveOptions}      autoplayInterval={3000} itemTemplate={Template}/>

            </div>


        </Wrapper>
    );
};


const Wrapper = styled.div`
    padding: 8rem 4rem;
    background-color: #000000;
    color: #ffffff;

    //@media (max-width: 658px) {
    @media (max-width: 760px) {
        padding: 8rem 1.5rem;
        .image {
            width: 100%;
            height: 50vh;
        }
    }

`


const CardContainer = styled.div`
    width: 95%;
    height: 25vh;
    margin-bottom: 0 !important;
    display: flex;
    flex-direction: column;
    text-align: start;
    border: 1px solid #0778FD;
    background-color: #000000;
    color: #FFFFFF;

    @media (max-width: 658px) {
        width: 100%;
        height: 35vh;

    }

`;


export default SectionTwo;
