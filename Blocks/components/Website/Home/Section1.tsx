import Image from "next/image";
import styled from "styled-components";
import reportingImg from "../../../../public/image/website/landing_page/reporting.svg";
import complianceImg from "../../../../public/image/website/landing_page/compliance.svg";
import aiImg from "../../../../public/image/page/Lingkaran1.png";
import Card_NSK from "../../../ReusedComponent/Card_NSK";
import {Button} from "primereact/button";
import React from "react";

const Items = [
    {
        subTitle:"DIGITAL",
        title: "Products",
        content: `At Nuxalle, we specialize in creating innovative digital products that solve real-world problems. Our team leverages cutting-edge technology and design principles to develop solutions that meet the unique needs of our clients. From conceptualization to deployment, 
        we ensure that every product we build is of the highest quality and delivers exceptional value.`,
        icon: reportingImg,
    },
    {
        subTitle:"IT",
        title: "Consulting",
        content: `Our IT consulting services are designed to empower businesses with the strategies and tools they need to thrive in a digital world. We offer expert guidance on technology adoption, digital transformation, and IT infrastructure optimization. By understanding your specific challenges and goals,
         we provide tailored solutions that enhance efficiency, productivity, and ROI.`,
        icon: complianceImg,
    },
    {
        subTitle:"DIGITAL",
        title: "Service",
        content: `Nuxalle’s digital services are crafted to elevate your brand and maximize your online presence. We provide a comprehensive range of services, including digital marketing, web development, and data analytics. Our approach is results-driven, ensuring that every campaign 
        and project we undertake delivers measurable outcomes and drives business growth.`,
        icon: aiImg,
    },

];

const SectionOne = () => {
    return (
        <Container className="relative text-700 lg:text-center lg:justify-content-center ">
            <div className="absolute top-0 left-0 lg:w-2 hidden md:block">
                <Image className="hidden md:block" src={aiImg} alt="Hero" width={130} height={250}/>
            </div>
            <div className="lg:text-center lg:justify-content-center lg:mb-8 mt-7">
                <h1 className="text-4xl font-bold mt-0 mb-0">
                    We do what it takes to move
                </h1>
                <h1 className="text-4xl font-bold mt-0 mb-3">
                    your business
                    <span className="text-blue-500 p-2">forward</span>
                </h1>

            </div>


            <InnerContainer>
                {Items.map((item, index) => (
                    <Card_NSK index={index} key={index} subTitle={item.subTitle} title={item.title} content={item.content}/>
                    // <Card_NSK index={index} key={index} title={item.title} content={item.content} icon={item.icon}/>
                ))}
            </InnerContainer>


            <div className="flex justify-content-center mt-8">
                <Button
                    label="Explore" icon="pi pi-arrow-right font-bold text-lg" iconPos="right"
                        style={{background: "#ffffff", color: '#000000',
                            border:"1px solid #0778FD", height: "3rem"}}/>
            </div>

        </Container>
    );
};

const Container = styled.div`
    background: #ffffff;
    //background: #D9D9D9;
    color: #000;
    padding: 7rem;
    line-height: 174%;
    @media (max-width: 760px) {
        padding: 1.5rem;
    }

`;
const InnerContainer = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
 
  gap: 2rem;
  @media (max-width:1000px) {
   grid-template-columns: 1fr 1fr;
   
  }
  @media (max-width:670px) {
   grid-template-columns: 1fr;
   gap: 1rem;
  }
`
export default SectionOne;
