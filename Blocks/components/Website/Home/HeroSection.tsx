import styled from 'styled-components';
import { useRouter } from 'next/navigation';

import HeroBackground from '../../../../public/image/website/landing_page/heroBackground.png';

import Image from 'next/image';
import { Button } from 'primereact/button';
import Overlay from '../../../../public/image/overlay.png';
import React from 'react';
import {CustomButton} from "../../../ReusedComponent/Styled_Button";



const HeroSection = () => {
    const router = useRouter();

    const NewRoute = () => {
        router.push('/register');
        console.log('clicked');
    };

    return (
        <div>
            <Container id="hero-section-container ">
                <div className="relative lg:mb-5 flex flex-wrap lg:p-8">
                    <div className="w-12 lg:w-6 p-2">
                        <h1 className="lg:text-6xl font-bold text-white mt-0 mb-3">
                            We are <span className="text-blue-500 p-2">always</span> here
                        </h1>
                        <h1 className="lg:text-6xl font-bold text-white mt-0 mb-3">
                             for all your needs
                        </h1>

                    </div>
                    <div className="w-12 lg:w-6 mt-4   overflow-hidden" style={{maxWidth: '500px'}}>
                        <p className="text-sm text-gray-400 mt-0 lg:mb-5 line-height-3  mx-auto" >
                            We are a digital tech agency with a globally dispersed team,
                            collaborating with clients around the world.
                            As passionate experts, we love building products
                            that are user-friendly and solve real problems.
                            Our commitment to excellence ensures that our services are always top-notch.
                        </p>
                        <p style={{ letterSpacing: '0.7em' }} className="lg:text-left text-start text-blue-500 ">/ GET STARTED</p>
                    </div>
                </div>
            </Container>

            <OverlayContainer>
                <OverlayImageContainer>
                    <Image
                        src={Overlay}
                        alt="Overlay"
                        layout="fill"
                        objectFit="cover"
                    />
                </OverlayImageContainer>
            </OverlayContainer>

            <Container id="hero-section-container">
                <div className=" flex flex-wrap lg:p-7 mt-5 lg:justify-content-center lg:text-center">
                    <div className="w-12 lg:w-12">
                        <h1 className="text-2xl font-bold text-white mt-4 mb-3">
                            Reimagine what’s possible with Nuxalle
                        </h1>

                       <div className="flex justify-content-center mt-6">
                           <Button label="About Us" icon="pi pi-arrow-right font-bold text-lg " iconPos="right"  style={{background:"#ffffff", color:'#000000', height:"3rem", border:"1px solid #0778FD"}}/>


                       </div>
                    </div>

                </div>
            </Container>
        </div>

    );
};

export default HeroSection;

const Container = styled.div`
    background-color: #000000;
    width: 100%;
    //position: relative;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    gap: 4rem;
    flex-direction: column;
 

    //@media (max-width: 1000px) {
    //    min-height: max-content;
    //}

    @media (max-width: 760px) {
        padding: 1.5rem;
    }
    
    //@media (max-width: 658px) {
    //    padding: 1rem;
    //}

`;

const OverlayContainer = styled.div`
    background-color: #D9D9D9;
    height: 150px;
    position: relative;
  
    @media (max-width: 992px) {
        display:none
    }
`


const OverlayImageContainer = styled.div`
    position: absolute;
    top: -50px; 
    left: 50%;
    transform: translateX(-50%);
    width: 85%;
    height: 250px;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;


  
`;

