"use client"
import React, {useContext, useRef} from "react";
import {useRouter} from "next/navigation";
import {Toast} from "primereact/toast";
import styled from "styled-components";
import { InferType } from 'yup';
import * as yup from 'yup';
import CustomForm, {IFormInput, IFormValues} from "../../../utils/Form/CustomForm";
import TextInput from "../../../utils/Inputs/TextInput";
import {LayoutContext} from "../../../../layout/context/layoutcontext";
import {Navigation_Style} from "../../../../styles/Styled_component/StyledComponent";
import Logo_Import from "../../../ReusedComponent/LogoIcon";
import {FormikHelpers} from "formik";
import {useMutation} from "@apollo/client";
import {FORGOT_USER_PASSWORD_MUTATION} from "../../../graphql/mutations/UserAuth";

import BackgroundImg from '../../../../public/image/website/sms2.jpg'


const inputList: Array<IFormInput> = [
    {
        name: 'email',
        as: TextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'email', className: 'w-full md:w-64 lg:w-96 mb-6 ' },
        placeholder: 'enter address',
        label: 'email address'
    },

];
const schema = yup.object({
    email: yup.string().email().required('registered Email is required'),
});

type InputPayLoad = InferType<typeof schema>;




const ForgotPassword  = ( ) =>{
    const { layoutConfig } = useContext(LayoutContext);
    const toast = useRef<any>(null);
    const router = useRouter();

    const [ForgotUserPassword, { loading }] = useMutation(FORGOT_USER_PASSWORD_MUTATION, {
        onCompleted: (data) => {
            const token = data?.forgotUserPassword?.resetToken
            const email = data?.forgotUserPassword?.email
            toast.current.show({ severity: 'success', summary: 'Email Verification',
                detail: `Verification Code has been sent to ${data?.forgotUserPassword?.email}`, life: 130000});
            // router.push(`/resetpassword?${token}`)
            router.push(`/resetpassword?token=${token}&email=${email}`);
        },
        onError: (error) => {
            console.error(error?.message, "error");
            toast.current.show({severity: 'warn', summary: 'Error', detail: `${error.message}`, life: 3000, });
        },
    })

    const handleSubmit = async (payload: IFormValues<IFormInput>, helpers: FormikHelpers<IFormValues<IFormInput>>) => {
        console.log(payload, "before execution")
        ForgotUserPassword({variables: {data:{...payload}}})
            .then((data) => {
                // helpers.resetForm();
            })
    };




    return(
        <>
            <Toast ref={toast} />
            <Container>
            <ImageContainer style={{backgroundImage:`url(${BackgroundImg.src})`}} >
            </ImageContainer>            
              
                 

                <FormContainer>
                    <InnerWrapper>
                        <Logo_Import/>
                        <div className="text-900 text-2xl font-medium mb-2">Forgot Password</div>
                        <span className="text-600 mr-2">Please enter your Registered email to recieve a code! </span>
                    </InnerWrapper>
                   <InnerWrapper>

                    <CustomForm
                        removeRestButton
                        enableReinitialize
                        initialValues={{ email: '' }}
                        validationSchema={schema}
                        onSubmitFunction={handleSubmit}
                        loading={loading}
                        formButtonProps={{
                            className: `w-full p-3 text-lg `
                        }}
                        submitButtonText="continue"
                        formInputs={inputList}
                    />
                                     </InnerWrapper>



                </FormContainer>
            </Container>

        </>
    )
}



const InnerWrapper=styled.div`

    width: 100%;
    max-width: 400px;
  
    padding:0 2rem;
`
const Container = styled.div`
    width: 100%;
    height: 100%;
  
    min-height: 100vh;
   
    display: grid;
    grid-template-columns: 1fr 1fr;
    @media (max-width:834px) {
       grid-template-columns : 1fr ;
    }
`
const ImageContainer = styled.div`
/* background-color: yellow; */
height: inherit;
height: 100%;
width: 100%;
background-position: left;
background-repeat: no-repeat;
background-size: cover;
@media (max-width:834px) {
     display: none;
    }
    
`
const FormContainer= styled.div`
    
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
const ForgotPasswordContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    font-size: 13px
`;



export default ForgotPassword