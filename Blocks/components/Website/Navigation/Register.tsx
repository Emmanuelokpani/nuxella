"use client"
import React, {useContext, useRef} from "react";
import {  useRouter } from "next/navigation";
import {Toast} from "primereact/toast";
import styled from "styled-components";
import { InferType } from 'yup';
import * as yup from 'yup';
import CustomForm, {IFormInput, IFormValues} from "../../../utils/Form/CustomForm";
import TextInput from "../../../utils/Inputs/TextInput";
import {LayoutContext} from "../../../../layout/context/layoutcontext";
import {Navigation_Style} from "../../../../styles/Styled_component/StyledComponent";
import Logo_Import from "../../../ReusedComponent/LogoIcon";
import {FormikHelpers} from "formik";
import {CREATE_USER_MUTATION} from "../../../graphql/mutations/UserAuth";
import {useMutation} from "@apollo/client";
import PasswordInput from '../../../utils/Inputs/PasswordInput';
import BackgroundImg from '../../../../public/image/website/img_1.png'


const inputList: Array<IFormInput> = [
    {
        name: 'fullName',
        as: TextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'name', className: 'w-full md:w-64 lg:w-96 ' },
        placeholder: 'full name',
        label: 'full name'
    },
    {
        name: 'phone',
        as: TextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'phone', className: 'w-full md:w-64 lg:w-96 ' },
        placeholder: 'phone number',
        label: 'phone number'
    },

    {
        name: 'email',
        as: TextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'email', className: 'w-full md:w-64 lg:w-96 ' },
        placeholder: 'email address ',
        label: 'email address'
    },

    {
        name: 'password',
        as: PasswordInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: {inputMode: 'password', className: 'lg:w-96', feedback: false },
        placeholder: 'password',
        label: 'password'
    },
    {
        name: 'userReferredCode',
        as: TextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'name', className: 'w-full md:w-64 lg:w-96 ' },
        placeholder: 'referral code',
        label: 'referral code'
    },


];


const schema = yup.object({
    fullName: yup.string().required('full name is required'),
    phone: yup.string().required('phone number is required'),
    email: yup.string().email().required('personal email is required'),
    password: yup.string().required('password is required'),
    userReferredCode: yup.string(),
});


type InputPayLoad = InferType<typeof schema>;



const Register = ( ) =>{
    const { layoutConfig } = useContext(LayoutContext);
    const toast = useRef<any>(null);
    const router = useRouter();

    const [CreateUser, { loading, data }] = useMutation(CREATE_USER_MUTATION, {
        onCompleted: (data) => {
            toast.current.show({ severity: 'success', summary: 'Registration Successful', detail: 'Thank you for registering! A verification code has been sent to your email Please use the code to complete the registration.', life: 3000});
            router.push(`/verify-email`)
        },
        onError: (error) => {
            console.error(error?.message, "error");
            toast.current.show({severity: 'warn', summary: 'Error', detail: `${error.message}`, life: 3000, });
        },
    })

    const handleSubmit = async (payload: IFormValues<IFormInput>, helpers: FormikHelpers<IFormValues<IFormInput>>) => {
        console.log(payload, "before execution")
        // const isValid = await schema.isValid(payload);
        // if (!isValid) {
        //     toast.current.show({ severity: 'warn', summary: 'all fields are required', detail: 'Please fill in all required fields.', life: 3000});
        //     console.log('Form is not valid. Please fill in all required fields.');
        //     return;
        // }

        CreateUser({variables: {data:{...payload}}})
            .then((data) => {
                console.log(data, "after execution")
            // helpers.resetForm();
        })
    };

    return(
        <>
            <Toast ref={toast} />
            <Container>
            <ImageContainer style={{backgroundImage:`url(${BackgroundImg.src})`}} >
            </ImageContainer>            
              
                <FormContainer>
                    <div className="mb-5">
                        <Logo_Import/>
                        <div className="text-900 text-2xl font-medium mb-1">Set up your account</div>
                        <span className="text-600 font-medium mr-2 text-sm">Already have an account?</span>
                        <a href='/login' className="font-medium no-underline text-orange-600 cursor-pointer">Login !</a>
                    </div>
                    <InnerWrapper>

                        <CustomForm
                            removeRestButton
                            enableReinitialize
                            initialValues={{ fullName:'', phone:'' , email: '', password: '', userReferredCode:""}}
                            onSubmitFunction={handleSubmit}
                            validationSchema={schema}
                            loading={loading}
                            formButtonProps={{
                                className: 'w-full p-3 text-lg'
                            }}
                            submitButtonText="sign up"
                            formInputs={inputList}
                        />
                    </InnerWrapper>


                </FormContainer>
            </Container>
        </>
    )
}



const InnerWrapper=styled.div`

    width: 100%;
    max-width: 400px;
  
    padding:0 2rem;
`
const Container = styled.div`
    width: 100%;
    height: 100%;
  
    min-height: 100vh;
   
    display: grid;
    grid-template-columns: 1fr 1fr;
    @media (max-width:834px) {
       grid-template-columns : 1fr ;
    }
`
const ImageContainer = styled.div`
/* background-color: yellow; */
height: inherit;
height: 100%;
width: 100%;
background-position: left;
background-repeat: no-repeat;
background-size: cover;
@media (max-width:834px) {
     display: none;
    }
    
`
const FormContainer= styled.div`
    
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
const ForgotPasswordContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    font-size: 13px
`;



export default Register