"use client"
import React, {useContext, useEffect, useRef, useState} from "react";
import {usePathname, useRouter, useSearchParams} from "next/navigation";
import {Toast} from "primereact/toast";
import styled from "styled-components";
import { InferType } from 'yup';
import * as yup from 'yup';
import CustomForm, {IFormInput, IFormValues} from "../../../utils/Form/CustomForm";
import TextInput from "../../../utils/Inputs/TextInput";
import {LayoutContext} from "../../../../layout/context/layoutcontext";
import {Navigation_Style} from "../../../../styles/Styled_component/StyledComponent";
import Logo_Import from "../../../ReusedComponent/LogoIcon";
import OTPTextInput from "../../../utils/Inputs/OTPInput";
import {FormikHelpers} from "formik";
import {useMutation} from "@apollo/client";
import {
    FORGOT_USER_PASSWORD_MUTATION,
    RESEND_USER_OTP_MUTATION,
    RESET_USER_PASSWORD_MUTATION
} from '../../../graphql/mutations/UserAuth';
import PasswordInput from '../../../utils/Inputs/PasswordInput';
import BackgroundImg from '../../../../public/image/website/sms2.jpg'
import { Button } from 'primereact/button';
import Image from 'next/image';
import FlutterPay from '../../../../public/image/dashboard/Home/flutterwave.svg';



const inputList: Array<IFormInput> = [
    {
        name: 'otp',
        as: OTPTextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'otp', className: 'lg:w-96' },
        placeholder: 'verification code',
        label: 'verification code'
    },

    {
        name: 'newPassword',
        as: PasswordInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { className: 'lg:w-96', feedback: false },
        placeholder: 'New password',
        label: 'new password'
    },

];

const schema = yup.object({
    otp: yup.string().required('Verification code is required'),
    newPassword: yup.string().required('new Password is required'),
});



type InputPayLoad = InferType<typeof schema>;


const ResetPassword = ( ) =>{
    const { layoutConfig } = useContext(LayoutContext);
    const toast = useRef<any>(null);
    const router = useRouter();

    const [token, setToken] = useState<any>("")
    const [email, setEmail] = useState<any>("")
    const searchParams = useSearchParams()


    const tokens = token?.slice(0, -1);
    const pathname = usePathname()


    // useEffect(() => {
    //     const url = `${searchParams}`
    //     console.log(url, "url");
    //     setToken(url)
    // }, [pathname, searchParams])


    useEffect(() => {
        const queryString = `${searchParams}`
        const paramsArray = queryString.split('&');
        const params = {};
        paramsArray.forEach(param => {
            const [key, value] = param.split('=');
            params[key] = decodeURIComponent(value);
        });
        const { token, email }:any = params;
        setToken(token), setEmail(email)
    }, []);



    const [ResetUserPassword, { loading }] = useMutation(RESET_USER_PASSWORD_MUTATION, {
        onCompleted: (data) => {
            toast?.current?.show({ severity: 'success', summary: 'password reset', detail: `password reset was successful`, life: 130000});
            console.log(data)
            router.push('/confirmed')
        },
        onError: (error) => {
            console.error(error?.message, "error");
            toast.current.show({severity: 'warn', summary: 'Error', detail: `${error.message}`, life: 3000, });
        },
    })


    const [ResendPasswordOTP, { loading:reset }] = useMutation(RESEND_USER_OTP_MUTATION, {
        onCompleted: (data) => {
            toast?.current?.show({ severity: 'success', summary: 'verification code', detail: `verification code has been resent successful`, life: 130000});
            console.log(data)
        },
    })


    const handleSubmit = async (payload: IFormValues<IFormInput>, helpers: FormikHelpers<IFormValues<IFormInput>>) => {
        console.log(payload, "before execution")
        ResetUserPassword({variables: {data:{...payload}}})
            .then((data) => {
                console.log(data);
                // helpers.resetForm();
            })
    };

    return(
        <>
            <Toast ref={toast} />
            <Container>
                <ImageContainer style={{ backgroundImage: `url(${BackgroundImg.src})` }}>
                </ImageContainer>


                <FormContainer>
                    <InnerWrapper>
                        <Logo_Import />
                        <div className="text-900 text-2xl font-medium mb-2">Reset Password</div>
                        <p className="text-600  text-sm">Please input the one time pass code what was sent to this email
                            address and Preferred password!</p>
                        <p className="text-600 mr-2 text-sm"></p>
                    </InnerWrapper>
                    <InnerWrapper>
                        <CustomForm
                            removeRestButton
                            enableReinitialize
                            initialValues={{ newPassword: '', otp: '', token: `${token}` }}
                            validationSchema={schema}
                            onSubmitFunction={handleSubmit}
                            loading={loading}
                            formButtonProps={{
                                className: `w-full p-3 text-lg `
                            }}
                            submitButtonText="reset password"
                            formInputs={inputList}
                        />
                    </InnerWrapper>

                    <p className="py-2 mt-2 text-900 text-sm "
                       style={{marginLeft:"10rem", cursor:"pointer"}}
                       onClick={() => {
                           if(!email){
                               toast?.current?.show({ severity: 'info', summary: 'Email', detail: `processing please wait`, life: 10000});
                               router.push("/forgotpassword")
                           }else {
                               ResendPasswordOTP({ variables: { data: { email: email } }, awaitRefetchQueries: true })
                                   .then((data: any) => {
                                       console.log(data)
                                   });
                           }

                       }}>
                        resend verification code
                    </p>
                </FormContainer>
            </Container>

        </>
    )
}

const InnerWrapper = styled.div`

    width: 100%;
    max-width: 400px;

    padding: 0 2rem;
`;
const Container = styled.div`
    width: 100%;
    height: 100%;

    min-height: 100vh;

    display: grid;
    grid-template-columns: 1fr 1fr;
    @media (max-width: 834px) {
        grid-template-columns: 1fr ;
    }
`;
const ImageContainer = styled.div`
    /* background-color: yellow; */
    height: inherit;
    height: 100%;
    width: 100%;
    background-position: left;
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 834px) {
        display: none;
    }
    
`
const FormContainer= styled.div`
    
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
const ForgotPasswordContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    font-size: 13px
`;


export default ResetPassword