"use client";
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import emailIcon from "../../../../public/image/website/landing_page/more/email.svg"
import phoneIcon from "../../../../public/image/website/landing_page/more/smartphone.svg"
import Logo from "../../../../public/image/nuxella.svg"
import Logo_dark from "../../../../public/image/nuxalle1.png"
import Image from "next/image";
import {useRouter} from "next/navigation";

const phoneNumber = "+234 809 420 0003";
const email = "info@kreativerock.com";

const NavigationMenu = ({ bgColor, Color }: { bgColor?: string, Color?: string }) => {

    const [menuOpen, setMenuOpen] = useState(false);
    const [isHeaderFixed, setIsHeaderFixed] = useState<any>(false);
    const router = useRouter();
    useEffect(() => {
      const handleScroll = () => {
        const heroSection = document.getElementById('hero-section-container');
        if(!heroSection)return
        const scrollPosition = window.scrollY;
  
        // Adjust the threshold value based on your design
        const threshold = heroSection.offsetHeight;
  
        setIsHeaderFixed(scrollPosition > threshold);
      };
  
      window.addEventListener('scroll', handleScroll);
  
      // Cleanup the event listener when the component unmounts
      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }, []);

    const toggleMenu = () => {
        setMenuOpen(!menuOpen);
    };

    const Register = ( )=>{
        router.push('/register')
    }
    const Contact = ( )=>{
        router.push('/contact')
    }
    return (
     <Wrapper  fixed={isHeaderFixed ? isHeaderFixed : undefined} bgColor={bgColor}  >

         <ContactDiv  fixed={isHeaderFixed ? isHeaderFixed : undefined}>

         </ContactDiv>
         <Container>
             <>
             {bgColor ? (
                 <Link href="/">
                     <Image
                         src={Logo_dark}
                         className='logo_dark mt-3'
                         alt="Logo"
                         height="25"

                     />
                 </Link>
             ):(
                 <Link href="/">
                     <Image
                         src={Logo}
                         className='logo mt-3'
                         alt="Logo"
                         height="25"

                     />
                 </Link>
             )}

             </>
             <MenuIcon onClick={toggleMenu}>
                 {bgColor ? (
                     <i className="pi pi-bars " style={{color:"#000000", fontSize:'1.4rem',fontWeight:600}} ></i>
                 ): (
                     <i className="pi pi-bars" style={{fontSize: '1.4rem', fontWeight: 600}}></i>
                 )}

             </MenuIcon>
             <NavCenter open={menuOpen}>
                             <CloseIcon onClick={toggleMenu}>

            <i className="pi pi-times " style={{color:"#000000", fontSize:'1.4rem',fontWeight:600}} ></i>

             </CloseIcon>
                 <NavItem Color={Color}>
                     <Link href="/about">About Us</Link>
                 </NavItem>

                 <NavItem Color={Color}>
                     <Link href="/service">Our Services</Link>
                 </NavItem>

                <NavItem Color={Color}>
                    <Link href="/manifesto">Our Manifesto</Link>
                </NavItem>

                 {/*<NavItem Color={Color}>*/}
                 {/*    <Link href="/blog">Blog</Link>*/}
                 {/*</NavItem>*/}

                 <NavEnd>
                 <ButtonL onClick={Contact}>
                     <a href="/contact">Contact Us</a>
                 </ButtonL>
               
               </NavEnd>
             </NavCenter>
            
         </Container>
     </Wrapper>
    );
};



 const ContactDiv = styled.div<{fixed:boolean}>`

  display: ${(props)=>props.fixed?'none':'block'};

  position:relative;
  background:transparent;
 
  
  
a{
  text-decoration:none;
  @media (max-width:500px) {
  .icon{
    height: .8rem;
    font-size: .8rem;
  }
}

}
   
`;


const Wrapper = styled.div<{ fixed: boolean, bgColor?: string }>`
    
  width: 100%;
  position: ${(props) => (props.fixed ? 'fixed' : 'relative')};
  top: ${(props) => (props.fixed ? '0' : 'auto')};
    background-color: ${(props:any) => props.bgColor || '#000000'};
  z-index: ${(props) => (props.fixed ? '1000' : 'auto')};
  box-shadow: ${(props) => (props.fixed ? '0 2px 5px rgba(0, 0, 0, 0.1)' : 'none')};
  left: ${(props) => (props.fixed ? '0' : 'auto')};
  padding: ${(props) => (props.fixed ? '4rem' : 'auto')};
  backdrop-filter: blur(10px);
    padding:0 4rem;
 


 
  @media (max-width: 658px) {
    padding:0 2rem;
  }
  /* Media query for max-width 500px */
  @media (max-width: 864px) {
    position: relative;
    top: auto;
    //background-color: #000000;
    z-index: auto;
    box-shadow: none;
    left: auto;
    padding: auto;
    backdrop-filter: none;
   
  }
`;



const Container = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  //background:linear-gradient(to right, #e7fffb, #ffdaa08c, #e7fffb); 
  background: transparent;
 
  //position: sticky;
  top: 0;
  @media (max-width:500px) {
    .logo{
      height: 40px;
    }
  }

`;


const MenuIcon = styled.div`
  display: none;
  flex-direction: column;
  justify-content: space-between;
  cursor: pointer;
  z-index: 2;
  color: #ffffff;
 font-size: 2rem;
  &.close{
   align-self: right;
  }
  @media (max-width: 864px) {
    display: flex;
      color: #ffffff;
  }
`;
const CloseIcon = styled.div`
  display: none;
  flex-direction: column;
  justify-content: space-between;
 
  align-self: flex-end;
  padding: 1rem;
  cursor: pointer;
  z-index: 2;
  color: #222 !important;
 font-size: 2rem;
 
  @media (max-width: 864px) {
    display: flex;
  }
`;


const NavCenter = styled.ul<{ open: boolean }>`
  display: flex;
  list-style: none;
  align-items: center;
  padding: 0;
  gap: 1rem;
  @media (max-width: 864px) {
    opacity: ${(props) => (props.open ? 1 : 0)};
    transition: all ease-in-out 1s;
    flex-direction: column;
    position: fixed;
    top: -1rem;
    left: ${(props) => (props.open ?0 : '-100%')};
    background: rgba(255,255,255,.8);
    //background: linear-gradient(to right, #e7fffb, #ffdaa08c, #e7fffb); 
    width: 100%;
    height: 100%;
    backdrop-filter: blur(10px);
    gap: 2rem;
    &*{font-size: 3rem;}
   
    text-align: start;
    z-index: 3;
    align-items: center;
    
    border-radius: 5px;
  }
`;

const NavEnd = styled.ul`
  display: flex;
  list-style: none;
  gap: 1rem;
  margin: 0;
  padding: 0;
  align-items: center;
  @media (max-width:864px) {
    flex-direction: column;
    gap: 2rem;
  }
 
`;


const NavItem= styled.li<{ Color?:string }>`

    //color: #ffffff;
    font-size: 15px;


    a {
        color: ${(props:any) => props.Color || '#ffffff'};
        font-weight: 600;
        text-decoration: none;

        &:hover {
            color: #119eda;
        }
        @media (max-width:864px) {
            color: #000000;
        }
    }


`;

const NavLink = styled.a<{ Color?:string }>`
  text-decoration: none;
    color: ${(props:any) => props.Color || '#ffffff'};
  font-weight:600;
  font-size: 12px;
  align-items: start;
 
  &:hover {
    text-decoration: none;
  }
  
 
`;

const Dropdown = styled.div`
  position: relative;
  /* display: inline-block;
  align-items: start; */
  margin: 0 1rem;
  @media (max-width: 864px) {
    padding-top:10px;
    margin-left:1.6rem
  }
`;

const DropdownContent = styled.div`
  position: absolute;
  top: 0;
  transition: all .5s;
  //background-color: #9a2e2e;
  background:#fff;
  border-radius: 5px;
  top: 100%;
  left: 0;
  width: 16rem;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  /* gap: 1rem; */
  visibility: hidden;
  //padding: 10px;
 opacity: 0;
  z-index: 99999;

  ${Dropdown}:hover & {
    visibility: visible;
    opacity: 1;
  }

  @media (max-width: 864px) {
    margin-top: 10px;
   
    /* position: relative; */
  }

`;

const DropdownItem = styled(NavLink)`
  display: block;
  padding: 8px;
  border-radius: 8px;
  margin: 8px;
    font-size: 13px;

  &:hover {
    background: #FF79004F;
   
  }
`;

const ButtonR = styled.button`
  width: 124.997px;
  height: 38.2px;
  flex-shrink: 0;
  border-radius: 23.584px;
  border: none;

  background: var(--Primary, #ff7900);
  cursor: pointer;
  a {
    color: #ffffff;
    text-decoration:none;
  }
`;

const ButtonL = styled.button`
  width: 124.997px;
  height: 38.2px;
  flex-shrink: 0;
  border-radius: 14px;
  border: none;
  cursor: pointer;
    
 

  a {
    color: #000000;
    text-decoration:none;
  }`

export default NavigationMenu;
