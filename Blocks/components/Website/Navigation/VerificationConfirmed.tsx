"use client"
import React, {useContext, useEffect, useRef} from "react";
import {useRouter} from "next/navigation";
import Confirmed from "../../../../public/image/website/confirm.svg"
import Image from "next/image";
import {Toast} from "primereact/toast";
import {LayoutContext} from "../../../../layout/context/layoutcontext";
import {Navigation_Style} from "../../../../styles/Styled_component/StyledComponent";
import Logo_Import from "../../../ReusedComponent/LogoIcon";
import {CustomButton} from "../../../ReusedComponent/Styled_Button";



const VerificationConfirmed = ( ) =>{
    const { layoutConfig } = useContext(LayoutContext);
    const toast = useRef<Toast | any>(null);
    const router = useRouter();

    const NewRoute = () => {
        router.push("/login");
    };
    useEffect(() => {
        const timer = setTimeout(() => {
            router.push("/login");
        }, 3000);
        return () => clearTimeout(timer);
    }, []);

    return(
        <>
            <Toast ref={toast} />
            <div className="flex h-full">
                <Navigation_Style className="hidden md:block w-6 bg-no-repeat bg-cover bg-bluee-500 relative"
                                  style={{ backgroundImage: "url('/image/website/sms2.jpg')",height:"50rem"  }}>
                </Navigation_Style>
                <div className="surface-section w-full md:w-6  md:p-8">
                        <Logo_Import/>
                    <div className="text-center capitalize p-3">
                        <p className="text-4xl font-medium text-900 font-bold mt-0"> Successful</p>
                        <Image src={Confirmed} alt='Confirmed' width={310} height={310} />
                        <div className='flex justify-content-center '>
                            <CustomButton
                                label={`login`}
                                className="custom-blue-button font-bold"
                                icon="add"
                                onClick={NewRoute}
                            />
                        </div>
                    </div>

                </div>
            </div>

        </>
    )
}


export default VerificationConfirmed