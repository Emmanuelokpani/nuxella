"use client"
import React, {useContext, useRef, } from "react";
import {useRouter} from "next/navigation";
import {Toast} from "primereact/toast";
import styled from "styled-components";
import { InferType } from 'yup';
import * as yup from 'yup';
import {LayoutContext} from "../../../../layout/context/layoutcontext";
import CustomForm, {IFormInput, IFormValues} from "../../../utils/Form/CustomForm";
import TextInput from "../../../utils/Inputs/TextInput";
import Logo_Import from "../../../ReusedComponent/LogoIcon";
import {FormikHelpers} from "formik";
import {useMutation} from "@apollo/client";
import {LOGIN_USER_MUTATION} from "../../../graphql/mutations/UserAuth";
import PasswordInput from '../../../utils/Inputs/PasswordInput';
import { parseDurationToMilliseconds } from '../../../../helpers/UserLoginExpiration';
import BackgroundImg from '../../../../public/image/website/sms2.jpg'


const inputList: Array<IFormInput> = [
    {
        name: 'email',
        as: TextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'email', className: 'w-full md:w-64 lg:w-96 mb-6 ' },
        placeholder: 'email address ',
        label: 'email address'
    },

    {
        name: 'password',
        as: PasswordInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { className: 'lg:w-96', feedback: false },
        // options: { inputMode: 'email', className: ' mb-3' },
        placeholder: 'password',
        label: 'password'
    },

];
const schema = yup.object({
    email: yup.string().email().required('personal Email is required'),
    password: yup.string().required('password is required'),
});

type InputPayLoad = InferType<typeof schema>;


const Login = ( ) =>{
    const { layoutConfig } = useContext(LayoutContext);
    const toast = useRef<any>(null);
    const router = useRouter();


    const [loginUser, { loading }] = useMutation(LOGIN_USER_MUTATION, {
        onCompleted: async (data) => {
            console.log(data, "data")
              const token = await data?.loginUser?.token;
              const expiresIn = await data?.loginUser?.tokenExpiration;

              localStorage?.setItem('token', token);

            if (expiresIn) {
                const expirationTime = Date.now() +  await parseDurationToMilliseconds(expiresIn);
                localStorage?.setItem('tokenExpiration', String(expirationTime));
                router.push("/home");
            }
        },
        onError: (error) => {
            console.error(error?.message, "error");
            toast.current.show({severity: 'warn', summary: 'Error', detail: `${error.message}`, life: 110000, });
            if(error.message === 'Please Verify Email to Continue'){
                router.push("/verify-email");
            }
        },
    })


    const handleSubmit = async (payload: IFormValues<IFormInput>, helpers: FormikHelpers<IFormValues<IFormInput>>) => {
   
        loginUser({variables: {data:{...payload}}})
            .then((data) => {
                console.log(data,"user data");
            })
    };

    const BeforeSubmitButton = () => {
        const Forgot = ()=>{
            router.push('/forgotpassword')
        }
        return (
            <ForgotPasswordContainer>
                Forgot your password?
                <p onClick={Forgot}
                   className="font-medium no-underline ml-2 text-left cursor-pointer"
                   style={{ color: '#FF7900'}}>Click here to reset
                </p>
            </ForgotPasswordContainer>
        );
    };

    return(
        <>
            <Toast ref={toast} />
            <Container>
            <ImageContainer style={{backgroundImage:`url(${BackgroundImg.src})`}} >
            </ImageContainer>            
              
                          
                <FormContainer>
                    <div className="mb-5">
                        <Logo_Import />
                        <div className="text-900 text-2xl font-medium mb-1">Login to Continue</div>
                        <span className="text-600 font-medium mr-2 text-sm">Don't have an account?</span>
                        <a href='/register' className="font-medium no-underline text-orange-600 cursor-pointer">Sign up!</a>
                    </div>

                    <InnerWrapper>
                        <CustomForm
                            removeRestButton
                            enableReinitialize
                            initialValues={{ email: '', password: '' }}
                            validationSchema={schema}
                            onSubmitFunction={handleSubmit}
                            loading={loading}
                            formButtonProps={{
                                className: `w-full p-3 text-lg `
                            }}
                            BeforeSubmitButton={<BeforeSubmitButton />}
                            // AfterSubmitButton={savedAccount && <AfterSubmitButton/>}
                            submitButtonText="login"
                            formInputs={inputList}
                        />
                    </InnerWrapper>

                </FormContainer>
            </Container>
        </>
    )
}
const InnerWrapper=styled.div`

    width: 100%;
    max-width: 400px;
  
    padding:0 2rem;
`
const Container = styled.div`
    width: 100%;
    height: 100%;
  
    min-height: 100vh;
   
    display: grid;
    grid-template-columns: 1fr 1fr;
    @media (max-width:834px) {
       grid-template-columns : 1fr ;
    }
`
const ImageContainer = styled.div`
/* background-color: yellow; */
height: inherit;
height: 100%;
width: 100%;
background-position: left;
background-repeat: no-repeat;
background-size: cover;
@media (max-width:834px) {
     display: none;
    }
    
`
const FormContainer= styled.div`
    
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
const ForgotPasswordContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    font-size: 13px
`;


export default Login