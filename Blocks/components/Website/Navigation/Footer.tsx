import { styled } from "styled-components";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import  Logo from "../../../../public/image/nuxella.svg"
import Image from "next/image";
import React from "react";

const Footer = ( )=>{
    return(
        <Container>
            <InnerContainer>
                <Navs>
                    <div className="flex flex-column p-2 ">
                        <span className=" text-2xl font-bold block mb-2 ">Links</span>
                        <ul className="list-none flex flex-column gap-4 p-0">
                            <li className='mt-2 flex flex-row  items-center justify-center'>

                                <a href='/about' className="hover:text-blue-200 mt-1 transition-duration-150 cursor-pointer text-xs  block uppercase font-bold">About Us</a></li>
                            <li className='mt-2 flex flex-row  items-center justify-center'>

                                <a href='/service'
                                   className=" hover:text-blue-200 mt-1 transition-duration-150 cursor-pointer text-xs  block uppercase font-bold">Our
                                    Services</a></li>
                            <li className='mt-2 flex flex-row  items-center justify-center'>

                                <a href='/manifesto'
                                   className=" hover:text-blue-200 mt-1 transition-duration-150 cursor-pointer text-xs  block uppercase font-bold">Our
                                    Manifesto</a></li>
                            <ButtonL>
                                <a href="/blog">Read Blog!</a>
                            </ButtonL>
                        </ul>
                    </div>
                </Navs>

                <div className="flex-column p-2">
                    <span className=" text-2xl font-bold block">Chat</span>
                    <ul className="list-none p-0 flex flex-column ">
                        <li><a
                            className=" text-sm transition-duration-150 cursor-pointer flex items-center justify-center gap-3 ">
                            <i className="pi pi-telegram text-2xl hover:text-blue-200"></i>
                            <i className="pi pi-whatsapp text-2xl hover:text-blue-200"></i>
                        </a></li>
                    </ul>

                    <span className=" text-lg font-bold block mt-6">Follow Us</span>
                    <ul className="list-none p-0 flex flex-column gap-4">
                        <li><a
                            className="text-sm transition-duration-150 cursor-pointer flex items-center justify-center gap-3 ">
                            <i className="pi pi-facebook text-2xl hover:text-blue-200"></i>
                            <i className="pi pi-twitter text-2xl hover:text-blue-200"></i>
                            <i className="pi pi-linkedin text-2xl hover:text-blue-200"></i>
                            <i className="pi pi-instagram text-2xl hover:text-blue-200"></i>
                        </a></li>

                    </ul>

                </div>

                <div className="p-2 sm:mt-0 flex-column">
                    <div className="flex justify-content-end">
                        <Image src={Logo} alt="Image" height={50} width={200} />
                    </div>

                    <span className="text-sm font-bold block mt-3 flex justify-content-end"> Reimagine what is possible</span>

                    <div className="mt-3 relative mx-auto mt-4">
                        <InputText
                            className="appearance-none   border-1 border-white-100 py-3 w-full p-component text-white outline-none"
                            style={{borderRadius: '5px', paddingRight: '6rem', background: "#0000"}} value=""
                            placeholder="Email Address"/>
                        <Button type="button" className="absolute text-white" label="Subcribe"
                                style={{
                                    borderRadius: '8px',
                                    top: '.5rem',
                                    bottom: '.5rem',
                                    right: '.5rem',
                                    background: "#3b82f6"
                                }}/>
                    </div>

                </div>
            </InnerContainer>

            <div className="flex flex-wrap mt-3 justify-content-between">
                <div className="w-full w-6">
                    <div className="text-white font-bold text-sm mb-3">
                        Privacy Policy | Terms of Use
                    </div>
                </div>
                <div className="w-full w-6 flex  justify-content-end">
                    <div className="text-white font-bold text-sm mb-3">
                        © 2024 Nuxalle. All rights reserved.
                    </div>
                </div>
            </div>

        </Container>
    )
}


const Container = styled.div`
    display: flex;
    background: #000000;
    color: #fff;

    a {
        text-decoration: none;
        color: #fff;
    }

    h2 {
        color: #fff;
    }

    flex-direction: column;
    padding: 2rem 3rem;

    @media (max-width: 658px) {
        padding: 1.5rem;
    }
`

const InnerContainer = styled.div`
    display: grid;
    grid-template-columns:1fr 1fr 1fr;
    margin: 4rem 0;
    margin-bottom: 0;
    gap: 4rem;
    @media (max-width: 966px) {
        gap: 3rem;
    }
    @media (max-width: 900px) {
        gap: 2rem;
        grid-template-columns:1fr;

    }
`
const Navs = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    gap: 4rem;

    @media (max-width: 966px) {
        gap: 3rem;
    }
    @media (max-width: 500px) {

        flex-direction: column;
        gap: 2rem;

    }
`

const ButtonL = styled.button`
  width: 124.997px;
  height: 38.2px;
  flex-shrink: 0;
  border-radius: 14px;
  border: none;
  cursor: pointer;
    
 

  a {
    color: #000000;
    text-decoration:none;
  }`

export default Footer