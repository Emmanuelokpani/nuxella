"use client"
import React, {useContext, useEffect, useRef, useState} from "react";
import {Toast} from "primereact/toast";
import { InferType } from 'yup';
import * as yup from 'yup';
import CustomForm, {IFormInput, IFormValues} from "../../../utils/Form/CustomForm";
import OTPTextInput from "../../../utils/Inputs/OTPInput";
import {LayoutContext} from "../../../../layout/context/layoutcontext";
import {Navigation_Style} from "../../../../styles/Styled_component/StyledComponent";
import Logo_Import from "../../../ReusedComponent/LogoIcon";
import {FormikHelpers} from "formik";
import {useMutation} from "@apollo/client";
import { VERIFY_USER_MUTATION} from "../../../graphql/mutations/UserAuth";
import {usePathname, useRouter, useSearchParams} from "next/navigation";


const inputList: Array<IFormInput> = [
    {
        name: 'otp',
        as: OTPTextInput,
        format: (value) => value.toLowerCase().replace(/\s/g, ''),
        options: { inputMode: 'otp', className: 'w-full md:w-64 lg:w-96 mb-6 ' },
        label:"verification code"
    },

];
const schema = yup.object({
    otp: yup.string().required('Verification code is required'),
});

type InputPayLoad = InferType<typeof schema>;




const VerifyEmail = ( ) =>{
    const { layoutConfig } = useContext(LayoutContext);
    const toast = useRef<any>(null);
    const router = useRouter();
    const [token, setToken] = useState<any>()

    const pathname = usePathname()
    const searchParams = useSearchParams()

    useEffect(() => {
        const url = `${searchParams}`
        setToken(url)
    }, [pathname, searchParams])


    const [VerifyEmailAccount, { loading }] = useMutation(VERIFY_USER_MUTATION, {
        onCompleted: (data) => {
            toast.current.show({ severity: 'success', summary: 'Email Verification',
                detail: `Email verified successfully`, life: 130000});
            console.log(data, "data")
            router.push('/confirmed')
        },
        onError: (error) => {
            console.error(error?.message, "error");
            toast.current.show({severity: 'warn', summary: 'Error', detail: `${error.message}`, life: 3000, });
        },
    })



    const handleSubmit = async (payload: IFormValues<IFormInput>, helpers: FormikHelpers<IFormValues<IFormInput>>) => {
        console.log(payload, "before execution")
        VerifyEmailAccount({variables: {data:{...payload}}})
            .then(() => {
                console.log(payload, "after execution")
                helpers.resetForm();
            })
    };



    return(
        <>
            <Toast ref={toast} />
            <div className="flex ">
                <Navigation_Style className="hidden md:block w-6 bg-no-repeat bg-cover bg-bluee-500 relative"
                                  style={{ backgroundImage: "url('/image/website/sms2.jpg')"}}>
                </Navigation_Style>

                <div className="surface-section w-full md:w-6 p-4 md:p-8 ">

                    <div className="mb-5">
                        <Logo_Import/>
                        <div className="text-900 text-3xl font-medium mb-3">Verify your email</div>
                        <span className="text-600 font-medium mr-2">Input the four digit pin sent to your email address to verify your account.?</span>
                    </div>

                     <div className='h-21rem'>
                         <CustomForm
                             removeRestButton
                             enableReinitialize
                             initialValues={{ otp: '' }}
                             validationSchema={schema}
                             onSubmitFunction={handleSubmit}
                             loading={loading}
                             formButtonProps={{
                                 className: 'lg:w-4 w-full p-3 text-lg'
                             }}
                             submitButtonText="verify"
                             formInputs={inputList}
                         />

                     </div>
                </div>
            </div>
        </>
    )
}



export default VerifyEmail