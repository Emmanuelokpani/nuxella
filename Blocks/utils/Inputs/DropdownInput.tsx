'use client';
import React, {ReactNode, useEffect, useState} from 'react';
import styled from 'styled-components';
import { InputText } from 'primereact/inputtext';
import {Dropdown} from "primereact/dropdown";
import {useQuery} from "@apollo/client";
import {USER_CONTACT_LIST} from "../../graphql/queries";
import {ContactGroup} from "../../../types/dashboard";
import {Nullable} from "primereact/ts-helpers";

type name = string;
type touched = { [key in name]: boolean };
type errors = { [key in name]: string };

export interface IInput {
    icon?: ReactNode;
    // inputStyle?: HTMLStyleElement;
    inputStyle?: React.CSSProperties;
    touched?: touched;
    name: name;
    value: string;
    formatValue?: (value: string) => string;
    errors?: errors;
    label?: string;
    placeholder: string;
    options?: any;
    onChange: (name: string, value: string | {}) => void;
    onExtraUpdate: (name: string, value: string | {}) => void;
}



const DropdownInput = ({ placeholder, onChange = () => undefined, value = '', options = {}, label, formatValue, name, errors = {} }: IInput) => {
    const {  loading, data } = useQuery(USER_CONTACT_LIST,
        { fetchPolicy: "network-only",   nextFetchPolicy: 'cache-and-network', },
    );
    const handleChange = (e: any, value: any) => {
       const newData = value
        onChange(name, formatValue ? formatValue(newData) : newData);
    };

    return (
        <Container>
            {label && (<label htmlFor={name} className="block text-900 text-md font-medium">{label}</label>)}
            <div className='lg:w-10 mt-3'>
                <Dropdown
                    name={name}   {...options}
                    id={name}
                    value={value}
                    options={data?.getAllContactGroups?.docs?.map((data: any) => ({ label: data.groupTitle, value: data._id }))}
                    onChange={(e) => handleChange("contactGroupId", e.value)}
                    className="w-full bg-gray-50 p-1"
                    placeholder={placeholder}
                />
            </div>
            <ErrorMessageContainer>
                <ErrorMessageDisplay>{ errors[name] ? errors[name] : null}</ErrorMessageDisplay>
            </ErrorMessageContainer>
        </Container>
    );
};


const Container = styled.div`
    input {
        margin-bottom: 3rem !important;
    }
`;
const ErrorMessageContainer = styled.div`
    margin-bottom: 0.7rem !important;
`;
const ErrorMessageDisplay = styled.div`
    color: rgba(144, 36, 36, 0.76);
    //text-transform: capitalize;
    font-size: 12px;
    position: relative;
`;
export default DropdownInput;
