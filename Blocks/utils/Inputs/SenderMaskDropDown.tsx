'use client';
import React, {ReactNode, useEffect, useState} from 'react';
import styled from 'styled-components';
import { InputText } from 'primereact/inputtext';
import {Dropdown} from "primereact/dropdown";
import {useQuery} from "@apollo/client";
import { USER_CONTACT_LIST, USER_SENDER_MASK } from '../../graphql/queries';
import {ContactGroup} from "../../../types/dashboard";
import {Nullable} from "primereact/ts-helpers";
import { classNames } from 'primereact/utils';

type name = string;
type touched = { [key in name]: boolean };
type errors = { [key in name]: string };

export interface IInput {
    icon?: ReactNode;
    // inputStyle?: HTMLStyleElement;
    inputStyle?: React.CSSProperties;
    touched?: touched;
    name: name;
    value: string;
    formatValue?: (value: string) => string;
    errors?: errors;
    label?: string;
    placeholder: string;
    options?: any;
    onChange: (name: string, value: string | {}) => void;
    onExtraUpdate: (name: string, value: string | {}) => void;
}

const category = [
    {_id:1, value:"oneWay", label:"one way sender mask"} ,
    // {_id:2, value:"twoWay", label:"two Way Sms"}
]


const SenderMaskDownInput = ({ placeholder, onChange = () => undefined, value = '', options = {}, label, formatValue, name, errors = {} }: IInput) => {



    const {  loading, data, error } = useQuery(USER_SENDER_MASK,
        { variables:{ page: 1, limit: 20, category: "twoWay"},
            fetchPolicy: "network-only",  nextFetchPolicy: 'cache-and-network',}
    );


    const handleChange = (e: any, value: any) => {
        const newData = value
        onChange(name, formatValue ? formatValue(newData) : newData);
        console.log(newData, "category");
    };

    return (
        <Container>
            {label && (<label htmlFor={name} className="block text-900 text-md font-medium">{label}</label>)}
            <div className='lg:w-12 mt-3 '>
                <Dropdown
                    name={name} {...options}
                    id={name}
                    value={value}
                    // className="w-full bg-gray-50 p-1"
                    placeholder={placeholder}
                    options={category} optionLabel="label"
                    onChange={(e) => handleChange("category", e.value)}
                    {...options}
                    className={classNames(options.className, {'p-invalid': errors[name]})}
                />
            </div>
            <ErrorMessageContainer>
                <ErrorMessageDisplay>{ errors[name] ? errors[name] : null}</ErrorMessageDisplay>
            </ErrorMessageContainer>
        </Container>
    );
};


const Container = styled.div`
    input {
        margin-bottom: 3rem !important;
    }
`;
const ErrorMessageContainer = styled.div`
    margin-bottom: 0.7rem !important;
`;
const ErrorMessageDisplay = styled.div`
    color: red;
    //text-transform: capitalize;
    font-size: 10px;
    margin-top:8px;
    position: relative;
`;
export default SenderMaskDownInput;
