'use client';
import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { InputText } from 'primereact/inputtext';
import {classNames} from "primereact/utils";
import {FormikErrors} from "formik/dist/types";

type name = string;
type touched = { [key in name]: boolean };
type errors = { [key in name]: string };

export interface IInput {
    icon?: ReactNode;
    inputStyle?: HTMLStyleElement;
    touched?: touched;
    setErrors: (errors: FormikErrors<any>) => void;
    name: name;
    value: string;
    formatValue?: (value: string) => string;
    errors?: errors;
    label?: string;
    placeholder: string;
    options?: any;
    onChange: (name: string, value: string | {}) => void;
    onExtraUpdate: (name: string, value: string | {}) => void;
}


const PhoneInput = ({ placeholder, onChange = () => undefined, value = '', options = {}, label, formatValue, name, errors = {} }: IInput) => {
    const [displayValue, setDisplayValue] = React.useState<string>(value);
    // const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    //     onChange(name, formatValue ? formatValue(e.target.value) : e.target.value);
    // };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const formattedValue = formatValue ? formatValue(e.target.value) : e.target.value;
        const sanitizedValue = formattedValue.replace(/^\+234/, '');
        setDisplayValue(sanitizedValue);
        onChange(name, `+234${sanitizedValue}`);
    };


    console.log(displayValue)
    return (
        <Container>
            {label && (
                <label htmlFor={name} className="block text-900 text-md font-medium mb-2">
                    {label}
                </label>
            )}
            <InputText
                placeholder={placeholder}
                onChange={handleChange}
                name={name} value={displayValue} id={name}
                type="text" style={{ padding: '20px' }}
                {...options}
                className={classNames(options.className, {'p-invalid': errors[name]})}
            />
            <ErrorMessageContainer>
                <ErrorMessageDisplay>{ errors[name] ? errors[name] : null}</ErrorMessageDisplay>
            </ErrorMessageContainer>
        </Container>
    );
};


const Container = styled.div`
    input {
        margin-bottom: 1.3rem !important;
    }
`;
const ErrorMessageContainer = styled.div`
    margin-bottom: 0.7rem !important;
`;
const ErrorMessageDisplay = styled.div`
    color: rgba(144, 36, 36, 0.76);
    //text-transform: capitalize;
    font-size: 12px;
    position: relative;
`;
export default PhoneInput;
