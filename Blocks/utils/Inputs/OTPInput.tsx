'use client';
import React, {ReactNode, useState} from 'react';
import styled from 'styled-components';
import { InputText } from 'primereact/inputtext';
import OtpInput from 'react-otp-input';
import {classNames} from "primereact/utils";

type name = string;
type touched = { [key in name]: boolean };
type errors = { [key in name]: string };

export interface IInput {
    icon?: ReactNode;
    inputStyle?: HTMLStyleElement;
    touched?: touched;
    name: name;
    value: string;
    formatValue?: (value: string) => string;
    errors?: errors;
    label?: string;
    placeholder: string;
    options?: any;
    props?: any;
    onChange: (name: string, value: string | {}) => void;
    onExtraUpdate: (name: string, value: string | {}) => void;
}

const OTPTextInput = ({ placeholder, onChange = () => undefined, value = '', options = {}, props = {}, label, formatValue, name, errors = {} }: IInput) => {

    const handlePaste: React.ClipboardEventHandler = (event) => {
        const data = event.clipboardData.getData('text');
        console.log(data)
    };
    const handleChange = (otp: string) => {
        console.log(otp, 'hi');
        onChange(name, formatValue ? formatValue(otp) : otp);
    };



    // @ts-ignore
    return (
        <Container>
            {label && (
                <label htmlFor={placeholder} className="block text-900 text-md mb-1">
                    {label}
                </label>
            )}
            <OtpInput
                inputStyle={classNames('p-invalid')}
                containerStyle={{ paddingBottom:"1px",}}
                inputType={"text"}
                value={value}
                onChange={handleChange}
                numInputs={4}
                renderSeparator={<span className='m-2'></span>}
                renderInput={(otp, index) => (
                    <InputText
                        name={`${name}-${index}`}
                        id={`${name}-${index}`}
                        pattern="[0-9]*"
                        // @ts-ignore
                        value={value[index] || ''}
                        // @ts-ignore
                        onChange={(event) => handleChange(event?.target?.value)}
                        {...otp}
                        {...options}
                        className="w-6 py-3  text-900 text-start"
                    />
                )}

            />
            <ErrorMessageContainer>
                <ErrorMessageDisplay>{ errors[name] ? errors[name] : null}</ErrorMessageDisplay>
            </ErrorMessageContainer>
        </Container>
    );
};


const Container = styled.div`
    input {
        margin-bottom: 0.6rem !important;
    }
`;
const ErrorMessageContainer = styled.div`
    margin-bottom: 1.5rem !important;
`;
const ErrorMessageDisplay = styled.div`
    //color: rgba(144, 36, 36, 0.76);
    //text-transform: capitalize;
    font-size: 10px;
    color:red;
    position: relative;
`;
export default OTPTextInput;
