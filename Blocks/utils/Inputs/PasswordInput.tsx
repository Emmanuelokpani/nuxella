'use client';
import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { Password } from 'primereact/password';
import { classNames } from 'primereact/utils';

type name = string;
type touched = { [key in name]: boolean };
type errors = { [key in name]: string };

export interface IInput {
    icon?: ReactNode;
    inputStyle?: HTMLStyleElement;
    touched?: touched;
    name: name;
    value: string;
    label?: string;
    formatValue?: (value: string) => string;
    errors?: errors;
    placeholder: string;
    options?: any;
    onChange: (name: string, value: string) => void;
}

const PasswordInput = ({ placeholder, onChange = () => undefined, value = '', options = {}, label, touched = {}, formatValue, name, errors = {} }: IInput) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(name, formatValue ? formatValue(e.target.value) : e.target.value);
    };
    return (
        <Container>
            {label && (
                <label htmlFor={placeholder} className="block text-900 text-md mb-1">
                    {label}
                </label>
            )}
            <Password inputId={name} value={value} name={name} placeholder={placeholder} onChange={handleChange} toggleMask {...options}
                      className={classNames(options.className, {'p-invalid': errors[name]})}
            />
            <ErrorMessageContainer>
                <ErrorMessageDisplay>{ errors[name] ? errors[name] : null}</ErrorMessageDisplay>
            </ErrorMessageContainer>
            {/*<ErrorMessageContainer>*/}
            {/*    <ErrorMessageDisplay>{touched[name] && errors[name] ? errors[name] : null}</ErrorMessageDisplay>*/}
            {/*</ErrorMessageContainer>*/}
        </Container>
    );
};

const Container = styled.div`
    margin-bottom: 0.8rem !important;
    flex: 1;
    .p-input-icon-right > .p-inputtext {
        width: 100% !important;

    }
    .p-password {
        margin-bottom: 0.7rem !important;
        width: 100% !important;
    }
`;
const ErrorMessageContainer = styled.div``;

const ErrorMessageDisplay = styled.div`
    //text-transform: capitalize;
    font-size: 10px;
    color: red;
    position: relative;
`;
export default PasswordInput;
