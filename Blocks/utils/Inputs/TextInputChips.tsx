'use client';
import React, {ReactNode, useState} from 'react';
import styled from 'styled-components';
import { InputText } from 'primereact/inputtext';
import {InputTextarea} from "primereact/inputtextarea";
import {Chips} from "primereact/chips";

type name = string;
type touched = { [key in name]: boolean };
type errors = { [key in name]: string };

export interface IInput {
    icon?: ReactNode;
    inputStyle?: HTMLStyleElement;
    touched?: touched;
    name: name;
    value: string;
    formatValue?: (value: string[]) => string;
    errors?: errors;
    label?: string;
    placeholder: string;
    options?: any;
    onChange: (name: string, value: string | {}) => void;
    onExtraUpdate: (name: string, value: string | {}) => void;
}

const TextInputChips = ({ placeholder, onChange = () => undefined, value = '', options = {}, label, formatValue, name, errors = {} }: IInput) => {

    const  [data, setData] =useState<any>([])

    // const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    //     onChange(name, formatValue ? formatValue(e.target.value) : e.target.value);
    // };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let formattedValue:any = "+234";
        if (Array.isArray(value)) {
            formattedValue = value.map((number) => `+234${number?.substring(1)}`);
            console.log(formattedValue, "as array")
        } else {
            formattedValue = `+234${value?.substring(1)}`;
        }
        let data:any = `+234${formattedValue}`
        onChange(name, formatValue ? formatValue : e.target.value);

        setData(formattedValue)
        console.log(formattedValue, "formattedValue");
    };

    return (
        <Container>
            {label && (
                <label htmlFor={name} className="block text-900 text-md font-medium mt-5 mb-2">
                    {label}
                </label>
            )}
            <span className='text-600 text-sm'>Please enter recipients' phone numbers. COMMA required between each phone number</span>
            <Chips
                rows={15}
                className="p-invalid text-sm text-500"
                placeholder={placeholder} onChange={handleChange} name={name} value={value} id={name} type="text"  {...options}
                separator=","  row={50}/>
            <ErrorMessageContainer>
                <ErrorMessageDisplay>{ errors[name] ? errors[name] : null}</ErrorMessageDisplay>
            </ErrorMessageContainer>
        </Container>
    );
};


const Container = styled.div`
    input {
        margin-bottom: 0.7rem !important;
    }
`;
const ErrorMessageContainer = styled.div`
    margin-bottom: 0.7rem !important;
`;
const ErrorMessageDisplay = styled.div`
    color: rgba(144, 36, 36, 0.76);
    //text-transform: capitalize;
    font-size: 12px;
    position: relative;
`;
export default TextInputChips;
