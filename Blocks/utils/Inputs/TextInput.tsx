'use client';
import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { InputText } from 'primereact/inputtext';
import {classNames} from "primereact/utils";
import {FormikErrors} from "formik/dist/types";

type name = string;
type touched = { [key in name]: boolean };
type errors = { [key in name]: string };

export interface IInput {
    icon?: ReactNode;
    inputStyle?: HTMLStyleElement;
    touched?: touched;
    setErrors: (errors: FormikErrors<any>) => void;
    name: name;
    value: string;
    formatValue?: (value: string) => string;
    errors?: errors;
    label?: string;
    placeholder: string;
    options?: any;
    onChange: (name: string, value: string | {}) => void;
    onExtraUpdate: (name: string, value: string | {}) => void;
}


const TextInput = ({ placeholder, onChange = () => undefined, value = '', options = {}, label, formatValue, name, errors = {} }: IInput) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(name, formatValue ? formatValue(e.target.value) : e.target.value);
    };

    return (
        <Container>
            {label && (
                <label htmlFor={placeholder} className="block text-900 text-sm mb-1">
                    {label}
                </label>
            )}
            <InputText
                placeholder={placeholder}
                onChange={handleChange}
                name={name} value={value} id={name}
                type="text" style={{ padding: '13px' }}
                {...options}
                className={classNames(options.className, {'p-invalid': errors[name]})}
            />
            <ErrorMessageContainer>
                <ErrorMessageDisplay>{ errors[name] ? errors[name] : null}</ErrorMessageDisplay>
            </ErrorMessageContainer>
        </Container>
    );
};


const Container = styled.div`
    margin-bottom: 0.6rem !important;
    input {
        margin-bottom: 0.6rem !important;
        font-size: 14px;
        .p-inputtext:enabled:hover{}
        :hover{
            outline: none !important;
            border: none !important;

        }
        :focus{
            outline: none !important;
            border: none !important;

        }
    }
    
`;
const ErrorMessageContainer = styled.div`
    margin-bottom: 1.5rem !important;
`;
const ErrorMessageDisplay = styled.div`
      color:red;
    //text-transform: capitalize;
    font-size: 10px;
    position: relative;
`;
export default TextInput;
