'use client'
import { LayoutProvider } from '../layout/context/layoutcontext';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css';
import '../styles/layout/layout.scss';
import '../styles/demo/Demos.scss';
import StyledComponentsRegistry from "../Blocks/utils/lib/styledComponentsRegistry";
import React from "react";
import { ApolloWrapper } from './ApolloWrapper';

interface RootLayoutProps {
    children: React.ReactNode;
}




export default function RootLayout({ children }: RootLayoutProps) {
    const title = "Nuxalle";

    return (
        <html suppressHydrationWarning>
        <head>
            <link id="theme-css" href={`/themes/lara-light-indigo/theme.css`} rel="stylesheet"></link>
            <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128 192x192" href="/favicon.ico"/>
            <link rel="icon" type="image/x-icon/png" href="/favicon.ico"/>
            <link rel="icon" type="image/png" href="/favicon.ico"/>
            <meta content="yes" name="apple-mobile-web-app-capable"/>
            <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
            <title>{title}</title>
            <meta charSet="UTF-8"/>
            <meta name="robots" content="index, follow"/>
            <meta name="mobile-web-app-capable" content="yes"/>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
            <link href="https://fonts.googleapis.com/css2?family=Archivo:wght@400;500;600;700&display=swap"
                  rel="stylesheet"/>
        </head>
        <body>
        <ApolloWrapper>
            <StyledComponentsRegistry>
                <LayoutProvider>{children}</LayoutProvider>
                    </StyledComponentsRegistry>
                </ApolloWrapper>
                </body>

        </html>
    );
}
