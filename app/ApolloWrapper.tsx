"use client"
import React, {useEffect, useRef, useState} from "react";
import {
    ApolloClient, ApolloError, ApolloLink, ApolloProvider,
    HttpLink,
    NormalizedCache
} from '@apollo/client';
import {ApolloNextAppProvider,NextSSRApolloClient, NextSSRInMemoryCache, SSRMultipartLink,} from "@apollo/experimental-nextjs-app-support/ssr";
import {API_URL, NEXT_PUBLIC_AUTH_KEY} from "../lib/url";
import {onError} from "@apollo/client/link/error";
import Head from "next/head";
import 'toastr/build/toastr.min.css'
import toastr from 'toastr'
import { toast, ToastContainer } from 'react-toastify';

const makeClient = ( )=> {

    const errorLink = onError((data) => {
        const {graphQLErrors, networkError, operation,} = data
        const operationDef = data.operation.query.definitions
        if (graphQLErrors) {
            operationDef.forEach((value) => {
                graphQLErrors.forEach(({message, locations, path}) => {
                    toast.error(message);
                    console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`, )
                });
            })
            return
        }
        if (networkError) {
            toast.error(`${networkError}`);
            console.log(`[GraphQL error]: Message: ${networkError},`)
        }
    });

    const isBrowser = typeof window !== 'undefined';
    const Token: any = typeof window !== 'undefined' && localStorage?.getItem('token')
    const token:any = isBrowser ? localStorage?.getItem('token') : null;

    const httpLink = new HttpLink({
        uri: `${API_URL}/graphql`,
        credentials: "include" ,
        fetchOptions: { cache: "no-store" },
        headers:{
            credentials: "include",
            authorization:token || `Bearer ${Token}`,
            'content-type': 'application/json',
        }
    });

    const link = ApolloLink.from([errorLink, new SSRMultipartLink({ stripDefer: true }), httpLink]);
    return new NextSSRApolloClient({
        cache: new NextSSRInMemoryCache(),
        ssrMode: false,
        link: typeof window === "undefined" ? link : httpLink,
    });

}



export function ApolloWrapper({ children}: React.PropsWithChildren) {

    const title = "Nuxalle";

    return (
        <>
            <Head>
                <link id="theme-css" href={`/themes/lara-light-indigo/theme.css`} rel="stylesheet"></link>
                <title>{title}</title>
            </Head>
            <ToastContainer style={{ zIndex: 9999 }} />
            <ApolloNextAppProvider makeClient={makeClient}>
                {/*{children}*/}
                <ApolloProvider client={makeClient()}>
                    {children}
                </ApolloProvider>
            </ApolloNextAppProvider>
        </>
    );
}





