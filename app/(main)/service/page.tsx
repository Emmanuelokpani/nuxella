"use client"
import SimpleLayout from "../layout";
import HeroSection from "../../../Blocks/components/Website/Service/HeroSection";
import Section1 from "../../../Blocks/components/Website/Service/Section1";
import FormData from "../../../Blocks/components/Website/Service/FormData";
import Section2 from "../../../Blocks/components/Website/Service/Section2";
import Section3 from "../../../Blocks/components/Website/Service/Section3";

const HomeLandingPage = () => {
    const navBarBgColor = "#ffffff";
    const navBarColor = "#000000";
    return (
        <SimpleLayout showNavBar={true} showFooter={true} navBarBgColor={navBarBgColor} navBarColor={navBarColor}>
          <HeroSection/>
          <Section1/>
          <Section2/>
          <Section3/>
          <FormData/>

        </SimpleLayout>
    );
};

export default HomeLandingPage;
