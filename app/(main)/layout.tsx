'use client'
import { Metadata } from 'next';
import React from 'react';
import Footer from "../../Blocks/components/Website/Navigation/Footer";
import NavigationMenu from "../../Blocks/components/Website/Navigation/NavigationMenu";
import StyledComponentsRegistry from "../../Blocks/utils/lib/styledComponentsRegistry";
import { ApolloWrapper } from '../ApolloWrapper';
// import {useRouter} from "next/navigation";



interface SimpleLayoutProps {
    children: React.ReactNode;
}


 const metadata: Metadata = {
    title: 'KreativeRock Digitals',
    description: 'The ultimate collection of design-agnostic, flexible and accessible React UI Components.'
};


const SimpleLayout = ({ children, showNavBar = false, showFooter = false, navBarBgColor, navBarColor}: any) => {

    return (
        <ApolloWrapper>
            <StyledComponentsRegistry>
                <React.Fragment>
                    <div style={{maxWidth:'1440px',width:"100%",margin:"0 auto"}}>

                    {showNavBar && <NavigationMenu bgColor={navBarBgColor} Color={navBarColor} />}
                    {children}
                    {showFooter && <Footer />}
                    </div>
                </React.Fragment>
            </StyledComponentsRegistry>
        </ApolloWrapper>
    );
}

export default SimpleLayout;
