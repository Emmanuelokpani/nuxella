"use client"

import SimpleLayout from "../layout";
import HeroSection from "../../../Blocks/components/Website/Manifesto/HeroSection";
import SectionOne from "../../../Blocks/components/Website/Manifesto/Section1";
import SectionTwo from "../../../Blocks/components/Website/Manifesto/Section2";
import SectionThree from "../../../Blocks/components/Website/Manifesto/Section3";

const HomeLandingPage = () => {
    return (
        <SimpleLayout showNavBar={true} showFooter={true}>
            <HeroSection/>
            <SectionOne/>
            <SectionTwo/>
            <SectionThree/>

        </SimpleLayout>
    );
};

export default HomeLandingPage;
