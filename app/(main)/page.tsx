"use client"
import SimpleLayout from "./layout";
import HeroSection from "../../Blocks/components/Website/Home/HeroSection";
import SectionTwo from "../../Blocks/components/Website/Home/Section2";
import SectionOne from "../../Blocks/components/Website/Home/Section1";
import SectionThree from "../../Blocks/components/Website/Home/Section3";

const HomeLandingPage = () => {
    return (
        <SimpleLayout showNavBar={true} showFooter={true}>
            <HeroSection />
            <SectionOne/>
            <SectionTwo/>
            <SectionThree/>
        </SimpleLayout>
    );
};

export default HomeLandingPage;
