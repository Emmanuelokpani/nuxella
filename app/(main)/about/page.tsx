"use client"

import SimpleLayout from "../layout";
import SectionTwo from "../../../Blocks/components/Website/About/Section2";
import SectionOne from "../../../Blocks/components/Website/About/Section1";
import SectionThree from "../../../Blocks/components/Website/About/Section3";
import TeamMates from "../../../Blocks/components/Website/About/Team";
import HeroSection from "../../../Blocks/components/Website/About/HeroSection";

const HomeLandingPage = () => {
    const navBarBgColor = "#ffffff";
    const navBarColor = "#000000";

    return (
        <SimpleLayout showNavBar={true} showFooter={true} navBarBgColor={navBarBgColor} navBarColor={navBarColor}>
            <HeroSection/>
            <SectionOne/>
            <SectionTwo/>
            <TeamMates/>
            <SectionThree/>

        </SimpleLayout>
    );
};

export default HomeLandingPage;
