"use client"

import SimpleLayout from "../layout";

const HomeLandingPage = () => {
    const navBarBgColor = "#ffffff";
    const navBarColor = "#000000";

    return (
        <SimpleLayout showNavBar={true} showFooter={true} navBarBgColor={navBarBgColor} navBarColor={navBarColor}>



        </SimpleLayout>
    );
};


export default HomeLandingPage;
