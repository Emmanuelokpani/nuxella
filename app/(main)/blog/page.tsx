"use client"

import SimpleLayout from "../layout";
import Blog_Post from "../../../Blocks/components/Website/Blog/Blog_Post";
import BlogGrid from "../../../Blocks/components/Website/Blog/BlogGrid";
import BlogGrid3 from "../../../Blocks/components/Website/Blog/BlogGrid3";

const HomeLandingPage = () => {
    const navBarBgColor = "#ffffff";
    const navBarColor = "#000000";

    return (
        <SimpleLayout showNavBar={true} showFooter={true} navBarBgColor={navBarBgColor} navBarColor={navBarColor}>
            <BlogGrid3/>
            <BlogGrid/>
            <Blog_Post/>
        </SimpleLayout>
    );
};

export default HomeLandingPage;
