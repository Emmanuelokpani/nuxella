import styled from "styled-components";


export const Hero_section = styled.div`
  background-size:cover;
  background: linear-gradient(to right,#E7FFFB, #FFDAA08C, #E7FFFB);
`;
export const Container = styled.div`
  //background: #F5F8FCCB;
  color: #000;
  padding-bottom: 60px;
  padding-top: 10px;
  //font-family: Lora;

  .Enquiry_text_header{
    color: #FFFDFB;
    font-weight: 700;
    font-size: 20px;
    position: absolute;
    top: 10%;
    left: 35%;
    transform: translate(-50%, -50%);
  }
  .Enquiry_text {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    color: #FFFDFB;
    font-size: 40px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    letter-spacing: 1.631px;
  }
  //Form_Input Style
  .PhoneInputInput {
    padding: 15px;
    border-radius: 10px;
    border-color: rgba(0, 0, 0, 0.12);
  }
  
  .feature_cards{
    border-radius: 16px;
    box-shadow: 0px 1px 2px 0px rgba(85, 105, 135, 0.10);
    margin:6px;
  }
  
  @media (max-width: 480px) {
    .feature_cards{
      width: 100%;
      max-width: 350px;

    }
  }

  @media (max-width: 1024px) {
    .feature_cards{
      width: 100%;
      max-width: 350px;

    }
  }
  
`


export const FooterStyle = styled.div`
   
    background: #03a387;
    color: #fff;
 

    .Footer_bottom {
        //background: #00CCA7;
        background: rgb(62, 101, 69)
    }

    a {
        text-decoration: none;
        color: #fff;
    }
    h2{
      color: #fff;
    }

    .wave-bg {
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: -1;
    }
`

export const Navigation_Style = styled.div`
  background-size:cover;

  .centered {
    position: absolute;
    top: 50%;
    left: 50%;
    //transform: translate(-50%, -50%);
  }
`;