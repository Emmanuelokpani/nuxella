
// SENDER MASK
export interface SenderMask {
    _id: string;
    smsMask: string;
    date: string;
    status: string;
}

// MEDIA MASK
export interface MediaMask {
    mediaId: string;
    mediaMask: string;
    userName: string;
    date: string;
    status: string;
}

//SMS DETAILS
export interface TotalSmsReportBar {
    sent: string;
    unitCost: string;
    success: string;
    fail: string;
    title: string;
}

export interface SmsReportData {
    reportId: string;
    senderMask: string;
    Recipient: string;
    content: string;
    status: string;
    cost: string;
    sendingTime: string;
    finishingTime: string;
}


// WALLET DETAILS
export interface Wallet {
    id: string;
    transactionId: string;
    name: string;
    amount: string;
    status: string;
    gateway: string;
    date: string;
}

// CONTACT GROUPING
export interface ContactGroup {
    _id: string;
    groupTitle: string;
    updatedAt: string;
}

export interface Contact {
    _id: string;
    groupTitle: string;
    phone:string,
}


//QUESTIONNAIRE
export interface QuestionnaireCampaign {
    id: string;
    name: string;
    date: string;
}

//VOICE
export interface VoiceFiles {
    voiceId: string;
    fileName:string;
    voiceFile?: File;
    date: string;
}

export interface TotalVoiceReportBar {
    sent: string;
    unitCost: string;
    success: string;
    fail: string;
    title: string;
}

export interface VoiceReportData {
    taskId:string,
    voiceId:string,
    campaignName:string,
    mediaMask:string,
    callingNumber:string,
    Recipient:string,
    content:string,
    status:string,
    cost:string,
    sendingTime:string,
    finishingTime:string,
}


// TWO - WAY - SMS
interface ChatMessage {
    sender: string;
    message: string;
    response?: string;
}

interface ChatEntry {
    name: string;
    phone: string;
    conversations: ChatMessage[];
}

export interface InteractiveChat extends Array<ChatEntry> {}