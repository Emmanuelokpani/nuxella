/** @type {import('next').NextConfig} */

const nextConfig = {

    compiler: {
        styledComponents: true,
    },
    typescript: {
        ignoreBuildErrors: true
    },
    reactStrictMode: true,

    devIndicators: {
        buildActivity: false
    },
    eslint: {
        ignoreDuringBuilds: true,
    },

}

module.exports = nextConfig