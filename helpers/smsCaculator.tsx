export function SMSCalculator(countMe: string): {page: number, count: number} {
    const escapedStr = encodeURI(countMe);
    const percentageCount = (escapedStr.match(/%/g) || []).length;
    const count = escapedStr.length - (percentageCount * 2);

    const page = count <= 160 ? Math.ceil(count / 160) : Math.ceil(count / 153);

    return { page, count };
}