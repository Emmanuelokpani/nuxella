import React from 'react';
import { GiCloudDownload } from 'react-icons/gi';

type CSVData = (string | null)[][];

function DownloadCSV() {
    const data: CSVData = [
        ['firstName', 'lastName', 'email', 'phone'],
        ['Lawrence', 'Nwoko', 'nwokolawrence1@gmail.com', '08163291415'],
    ];

    const makeCSV = (content: CSVData): string => {
        let csv = '';
        content.forEach((value) => {
            value.forEach((item, i) => {
                let innerValue = item === null ? '' : item.toString();
                let result = innerValue.replace(/"/g, '""');
                if (result.search(/('|,|\n)/g) >= 0) {
                    result = '"' + result + '"';
                }
                if (i > 0) {
                    csv += ',';
                }
                csv += result;
            });
            csv += '\n';
        });
        return csv;
    };

    const handleDownload = () => {
        const csvContent = makeCSV(data);
        const blob = new Blob([csvContent], { type: 'application/csv;charset=utf-8;' });
        const link = document.createElement('a');
        if (link.download !== undefined) {
            const url = URL.createObjectURL(blob);
            link.setAttribute('href', url);
            link.setAttribute('download', 'Kreative-Digital.csv');
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    };

    return (
        <div>

            <button
                className="flex items-center justify-center px-5 py-2 gap-2 text-sm border-round-lg border-none gap-x-2  mt-4 mb-4 cursor-pointer"
                type="submit"
                onClick={handleDownload}
                style={{ backgroundColor:"#d9f3d9"}}>
                Download CSV Sample  <GiCloudDownload className="text-1xl animate-bounce" />
            </button>
        </div>
    );
}

export default DownloadCSV;
