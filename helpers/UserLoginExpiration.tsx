// export const parseDurationToMilliseconds = (durationString:any)  => {
//     const regex = /^(\d+)h$/;
//     const match = durationString.match(regex);
//
//     if (!match) {
//         throw new Error(`Invalid duration format: ${durationString}`);
//     }
//     const hours = parseInt(match[1], 10);
//     const millisecondsPerHour = 60 * 60 * 1000;
//
//     return hours * millisecondsPerHour;
// };

export const parseDurationToMilliseconds = async (durationString: string): Promise<number>  => {
    return new Promise((resolve, reject) => {
        const regex = /^(\d+)h$/;
        const match = durationString.match(regex);

        if (!match) {
            reject(new Error(`Invalid duration format: ${durationString}`));
        }
        const hours = parseInt(match[1], 10);
        const millisecondsPerHour = 60 * 60 * 1000;
        setTimeout(() => {
            resolve(hours * millisecondsPerHour);
        }, 0);
    });
};

