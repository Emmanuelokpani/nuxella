import React, { useState } from "react";
import axios, { AxiosError, AxiosProgressEvent, AxiosResponse } from "axios";

interface Fields {
    [key: string]: string;
}

interface PresignedData {
    url: string;
    fields: Fields;
}

interface IUploader {
    onCompleted?: (res: AxiosResponse) => void;
    onError?: (error: AxiosError) => void;
}

const defaults = {
    onCompleted: (res: AxiosResponse) => {},
    onError: (error: AxiosError) => {},
}

const useUploader = ({ onCompleted, onError }: IUploader = defaults) => {
    const [error, setError] = useState<AxiosResponse | AxiosError>();
    const [uploadPercentage, setUploadPercentage] = useState(0); // State for Upload Progress
    const [loading, setLoading] = useState(false)

    const handleReset = () => {
        setError(undefined)
        setUploadPercentage(0)
    }

    const addHyphensToKeys = (obj: Fields): Fields => {
        return Object.entries(obj).reduce((acc, [key, value]) => {
            let newKey = key.replace(/(Amz)/g, "-$1-");
            return { ...acc, [newKey]: value };
        }, {}) as Fields;
    };

    const upload = async (data: Object & PresignedData, file: File|null) => {
        let formData = new FormData();
        delete data.fields.__typename
        // console.log( addHyphensToKeys(data.fields))
        const formatted = {
            url: data.url,
            fields: addHyphensToKeys(data.fields)
        }

        Object.keys(formatted.fields).forEach((key) => {
            if(key !== "id" && key !=="_typename"){
                formData.append(key, formatted.fields[key]);
            }
        });

        if (file) {
            formData.append("file", file);
        }
        try {
            setLoading(true)
            const result = await axios({
                method: "post",
                url: data.url,
                data: formData,
                headers: {
                    "Content-Type": "multipart/form-data",
                },
                onUploadProgress: (progressEvent: AxiosProgressEvent) => {
                    const total: number = progressEvent.total || 0;
                    const percentCompleted = Math.round(
                        (progressEvent.loaded * 100) / total
                    );
                    setUploadPercentage(percentCompleted);
                },
            });

            if (result.status === 204) {
                onCompleted?.(result.data);
                console.log("File uploaded successfully");
            } else {
                setError(result)
                onError?.(result as unknown as AxiosError);
                console.log("Failed to upload file");
            }
        } catch (error: AxiosError | any) {
            setError?.(error.response);
            onError?.(error.response);
        } finally {
            setLoading(false)
        }
    };
    return { uploadPercentage, upload,handleReset, error , loading};
};

export default useUploader


// import React, {useState} from "react";
// import axios, {AxiosError, AxiosProgressEvent, AxiosResponse} from "axios";
//
// interface Fields {
//     [key: string]: string;
// }
//
// interface PresignedData {
//     url: string;
//     fields: Fields;
// }
//
// interface IUploader {
//     onCompleted?: (res: AxiosResponse) => void;
//     onError?: (error: AxiosError) => void;
// }
//
// const defaults = {
//     onCompleted: (res: AxiosResponse) => {
//     },
//     onError: (error: AxiosError) => {
//     },
// }
//
// export const useUploader = ({onCompleted, onError}: IUploader = defaults) => {
//     const [error, setError] = useState<AxiosResponse | AxiosError>();
//     const [uploadPercentage, setUploadPercentage] = useState(0); // State for Upload Progress
//
//
//     const handleReset = () => {
//         setError(undefined)
//         setUploadPercentage(0)
//     }
//
//     const addHyphensToKeys = (obj: Fields): Fields => {
//         return Object.entries(obj).reduce((acc, [key, value]) => {
//             let newKey = key.replace(/(Amz)/g, "-$1-");
//             return {...acc, [newKey]: value};
//         }, {}) as Fields;
//     };
//
//     const upload = async (data: Object & PresignedData, file: File | null) => {
//         let formData = new FormData();
//
//         const formatted = {
//             url: data.url,
//             fields: addHyphensToKeys(data.fields)
//         }
//
//         Object.keys(formatted.fields).forEach((key) => {
//             if (key === "_id") return;
//             if (key === "__typename") return;
//             formData.append(key, formatted.fields[key]);
//         });
//
//         if (file) {
//             formData.append("file", file);
//         }
//
//         try {
//             const result = await axios({
//                 method: "post",
//                 url: data.url,
//                 data: formData,
//                 headers: {
//                     "Content-Type": "multipart/form-data",
//                 },
//                 onUploadProgress: (progressEvent: AxiosProgressEvent) => {
//                     const total: number = progressEvent.total || 0;
//                     const percentCompleted = Math.round(
//                         (progressEvent.loaded * 100) / total
//                     );
//                     setUploadPercentage(percentCompleted);
//                 },
//             });
//
//             if (result.status === 204) {
//                 onCompleted?.(result.data);
//                 console.log("File uploaded successfully");
//             } else {
//                 setError(result)
//                 onError?.(result as unknown as AxiosError);
//                 console.log("Failed to upload file");
//             }
//         } catch (error: AxiosError | any) {
//             setError?.(error.response);
//             onError?.(error.response);
//         }
//     };
//     return {uploadPercentage, upload, handleReset, error};
// };
//
