/* eslint-disable @next/next/no-img-element */

import React, { useContext } from 'react';
import { LayoutContext } from './context/layoutcontext';
import Link from 'next/link';
import Image from 'next/image';
import Logo from '../public/image/logo-dark.svg';

const AppFooter = () => {
    const { layoutConfig } = useContext(LayoutContext);

    return (
        <div className="layout-footer">
                    <Image
                        src={Logo}
                        alt="Logo"
                        height="40"
                        className="mr-2 lg:ml-6"
                    />
            {/*<img src={`/layout/images/logo-${layoutConfig.colorScheme === 'light' ? 'dark' : 'white'}.svg`} alt="Logo" height="20" className="mr-2" />*/}
            {/*<span className="font-medium ml-2">kreative-Rock</span>*/}
        </div>
    );
};

export default AppFooter;
