/* eslint-disable @next/next/no-img-element */

import React, {useContext, useEffect, useState} from "react";
import AppMenuitem from "./AppMenuitem";
import { LayoutContext } from "./context/layoutcontext";
import { MenuProvider } from "./context/menucontext";
import { AppMenuItem } from "../types/types";
import {useRouter, usePathname, } from "next/navigation";

import sms1 from "../public/image/dashboard/SMS/mask.svg"
import sms2 from "../public/image/dashboard/SMS/sms1.svg"
import sms3 from "../public/image/dashboard/SMS/sms2.svg"

import voice1 from "../public/image/dashboard/voice/voice1.svg"
import voice2 from "../public/image/dashboard/voice/voice2.svg"
import voice3 from "../public/image/dashboard/voice/voice3.svg"
import voice4 from "../public/image/dashboard/voice/voice4.svg"

import ques1 from "../public/image/dashboard/Question/questions.svg"
import ques2 from "../public/image/dashboard/Question/question-folder1.svg"
import ques3 from "../public/image/dashboard/Question/question1.svg"
import ques4 from "../public/image/dashboard/Question/question2.svg"
import ques5 from "../public/image/dashboard/Question/question3.svg"
import { GoRepoTemplate } from 'react-icons/go';




type SubMenuItem = {
    label: string;
    icon: string;
    to: string;
    id: number;
    separator?: boolean;
};

type MenuItemSub = {
    label?: string;
    separator?: boolean;
    items: SubMenuItem[];
};


type MenuItem = {
    label: string;
    separator?: boolean;
    items: (SubMenuItem | MenuItemSub)[];
};

type MenuModels = {
    [key: string]: MenuItem[];
};

const getSelectedRootRoute = (path:any) => {
    const match = path.match(/^\/home\/([a-zA-Z]+)/);
    return match ? match[1] : "sms";
};




// icon: sales_dark.src,
const getMenuModel = (rootRoute:any) => {
    const menuModels:MenuModels = {
        sms: [
            {
                label: "mobile texting service",
                items: [
                    { label: "sender mask", icon: sms1.src, to: "/home/sms" , id:1},
                    { label: "sms message", icon: sms2.src, to: "/home/sms/send", id:12 },
                    { label: "message record", icon: sms3.src, to: "/home/sms/report", id:15 },
                ],
            },
        ],
        interactiveSms: [
            {
                label: "RCS conversation service",
                items: [
                    { label: "sender mask", icon:sms1.src, to: "/home/interactiveSms" , id:1},
                    { label: "campaign", icon:sms1.src, to: "/home/interactiveSms/campaign" , id:1},
                    { label: "chat  report", icon:  sms3.src, to: "/home/interactiveSms/report", id:4 },
                    // { label: "chat message", icon:sms2.src, to: "/home/interactiveSms/sms" , id:2},
                    // { label: "chat conversation", icon:sms2.src, to: "/home/interactiveSms/conversation" , id:3},
                ],
            },
        ],

        voice:[
            {
                label: "Voice",
                items: [
                    { label: "sender mask", icon: sms1.src, to: "/home/voice" , id:1},
                    { label: "Bulk Voice Call", icon: voice2.src, to: "/home/voice/send" , id:2},
                    // { label: "Caller ID", icon:  voice1.src, to: "/home/voice" , id:1},
                    // { label: "Bulk Voice Call Record", icon: voice4.src, to: "/home/voice/record" , id:4},
                ],
            },
        ],
        whatsapp:[
            {
                label: "whatsapp campaign",
                items: [
                    { label: "Sender Mask", icon:ques1.src, to: "/home/whatsapp" , id:1},
                    { label: "New template", icon: ques4.src, to: "/home/whatsapp/new_templates" , id:2},
                    { label: "templates", icon: ques2.src, to: "/home/whatsapp/templates" , id:3},

                    { label: "campaign", icon: ques3.src, to: "/home/whatsapp/new" , id:4},
                    // { label: "campaigns", icon: ques2.src, to: "/home/whatsapp/campaigns" , id:5},

                    // { label: "campaign", icon: ques2.src, id:2,
                    //     items: [
                    //
                    //         { label: "new campaign", icon: ques3.src, to: "/home/whatsapp/new" , id:1},
                    //         { label: "new campaign", icon: ques3.src, to: "/home/whatsapp/new" , id:1},
                    //     ],
                    // },
                    { label: "campaign reports", icon: ques5.src, to: "#", id:4 },
                ],
            },
        ]
    };

    return menuModels[rootRoute] || [];
};


const AppMenu = () => {
    const { layoutConfig } = useContext(LayoutContext);
    const router = useRouter();

    const currentPath = usePathname();

    const selectedRootRoute = getSelectedRootRoute(currentPath);
    const menuModel = getMenuModel(selectedRootRoute);
    // // const menuModel = menuModels[currentPath] || menuModels.conversation;

    // const pathArray = currentPath.split('/home').filter((item: string) => item);
    // const key = pathArray.join('/home');
    // const menuModel = menuModels[key] || [];


    return (
        <MenuProvider>
            <ul className="layout-menu">
                {menuModel?.map((item:any, i:any) => {
                    return !item?.separator ? (
                        <AppMenuitem item={item} root={true} index={i} key={i} />
                    ) : (
                        <li className="menu-separator" key={`separator-${i}`}></li>
                    );
                })}
            </ul>
        </MenuProvider>
    );
};



export default AppMenu;

