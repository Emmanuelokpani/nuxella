
import * as Yup from "yup";
import { object, array } from "yup";

const URL = /^((https?|ftp):\/\/)?(www.)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i

export const paymentValidationSchema = Yup.object().shape({
    amount: Yup.number()
        .typeError(('The value must be a number'))
        .integer(('The value must be a number'))
        .required(('Amount is required')),

})


enum campaignType {
    sms = "sms",
    whatsApp = "whatsApp",
    voice = "voice",
}

export const TwoSmsValidationSchema = Yup.object().shape({
    name: Yup.string().required('campaign title is required'),
    category: Yup.string().oneOf(['twoWay'], 'Invalid category').required('Category is required'),
    contactGroupId: Yup.string().required('Contact Group ID is required'),
    smsMask: Yup.string().required('conversation Mask is required'),
    message: Yup.string().required('message  is required'),
    type: Yup.string().oneOf(['sms', "whatsApp"], 'Invalid type').required('Type is required'),
})



export const OneSmsValidationSchema = Yup.object().shape({
    name: Yup.string().required('campaign title is required'),
    category: Yup.string().oneOf(['oneWay'], 'Invalid category').required('Category is required'),
    contactGroupId: Yup.string().required('Contact Group ID is required'),
    smsMask: Yup.string().required('conversation Mask is required'),
    message: Yup.string().required('message  is required'),
    type: Yup.string().oneOf(['voice'], 'Invalid type').required('Type is required'),
})

// category: Yup.string()
//     .required('category is required')
//     .oneOf(['oneWay', 'twoWay']),
// type: Yup.mixed<campaignType>().oneOf(Object.values(campaignType)),

export const MediaValidationSchema = Yup.object().shape({
    name: Yup.string().required('campaign title is required'),
    category: Yup.string().oneOf(['oneWay'], 'Invalid category').required('Category is required'),
    contactGroupId: Yup.string().required('Contact Group ID is required'),
    mediaMask: Yup.string().required('Media Mask is required'),
    message: Yup.string().required('message  is required'),
    type: Yup.string().oneOf(['voice'], 'Invalid type').required('Type is required'),

})




export const createUserEnquirySchema = Yup.object().shape({
    fullName: Yup.string() .required("Full Name is Required"),
    organizationName: Yup.string().required("Organizations Name is Required"),
    email: Yup.string().email().required("Email is required"),
    website: Yup.string().url('Invalid URL').required('Website is required'),
    descriptions: Yup.string().required("Service Description is required"),
    service: Yup.string().required('Service is required'),

    // websiteUrl: Yup.string().required("Website  url is required").matches(URL, 'Enter a valid url'),
    // phone: Yup.number()
    //     .typeError("That doesn't look like a phone number")
    //     .positive("A phone number can't start with a minus")
    //     .integer("A phone number can't include a decimal point")
    //     .min(8,"phone Number is inValid")
    //     .required('phone Number is required'),
});


